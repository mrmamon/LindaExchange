//
//  HomeTableViewCell.swift
//  LindaExchange
//
//  Created by Milion Sun on 28/8/60.
//  Copyright © พ.ศ. 2560 LindaExchange. All rights reserved.
//

import UIKit

class HomeTableViewCell: UITableViewCell {
    @IBOutlet var flagImageView: UIImageView!
    @IBOutlet var nameLabel: UILabel!
    @IBOutlet var buyLabel: UILabel!
    @IBOutlet var sellLabel: UILabel!
    @IBOutlet var countryLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.layer.borderWidth = 0.5
        self.layer.borderColor = UIColor(red: 146.0 / 255.0, green: 148.0 / 255.0, blue: 151.0 / 255.0, alpha: 1.0).cgColor
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
