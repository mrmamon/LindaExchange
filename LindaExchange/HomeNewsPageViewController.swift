//
//  HomeNewsPageViewController.swift
//  LindaExchange
//
//  Created by Milion Sun on 11/8/2560 BE.
//  Copyright © 2560 LindaExchange. All rights reserved.
//

import UIKit
import Localize_Swift
import Kingfisher

class HomeNewsPageViewController: UIViewController {
    @IBOutlet var newsImageView: UIImageView!
    @IBOutlet var newsLabel: UILabel!
    @IBOutlet var newsLabelView: UIView!
    var news: NewsDB?
    var hiding = true
    var language = 0
    var imageset = false
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        showNewsDetail(false)
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.newsImageTapped(gesture:)))
        self.view.addGestureRecognizer(tap)
        NotificationCenter.default.addObserver(self, selector: #selector(setText), name: NSNotification.Name( LCLLanguageChangeNotification), object: nil)
        
        if !imageset {
            self.setImage()
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        setText()
        showNewsDetail(true)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        showNewsDetail(false)
    }
    
    func setNews(_ news: NewsDB) {
        self.news = news
        if let label = self.newsLabel {
            label.text = news.topic[language]
        }
        
        self.setImage()
    }
    
    func setPageLanguage(_ lang: Int) {
        if self.language != lang {
            self.language = lang
            self.newsLabel.text = news?.topic[language]
        }
    }

    func downloadImage(url: URL, imageView: UIImageView) {
        imageView.kf.setImage(with: url)
    }
    
    func showNewsDetail(_ show: Bool) {
        if show && hiding {
            UIView.animate(withDuration: 0.5) {
                self.newsLabelView.frame.origin.y -= self.newsLabelView.frame.size.height
            }
            self.hiding = false
        } else if !show && !self.hiding {
            self.newsLabelView.frame.origin.y += self.newsLabelView.frame.size.height
            self.hiding = true
        }
    }
    
    @objc func newsImageTapped(gesture : UITapGestureRecognizer) {
        if let news = self.news {
            let viewController = self.storyboard?.instantiateViewController(withIdentifier: "NewsDetail") as! NewsViewController
            viewController.news = news
            
            self.navigationController?.pushViewController(viewController, animated: true)
        }
    }
    
    @objc func setText() {
        if let news = self.news {
            if let label = self.newsLabel {
                label.text = news.topic[AppState.sharedInstance.currentLanguage.rawValue]
            }
            self.setImage()
        }
    }
    
    func setImage() {
        if let news = self.news {
            var url = news.photo[language]
            if !isEscaped(url) {
                url = url.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
            }
            if let imageView = self.newsImageView,
                let checkedUrl = URL(string: url) {
                self.downloadImage(url: checkedUrl, imageView: imageView)
            }
        }
    }
    
    func isEscaped(_ url: String) -> Bool {
        return url != url.removingPercentEncoding
    }
}
