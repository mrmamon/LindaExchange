//
//  BranchViewController.swift
//  LindaExchange
//
//  Created by Milion Sun on 8/30/2560 BE.
//  Copyright © 2560 LindaExchange. All rights reserved.
//

import UIKit
import GoogleMaps
import FirebaseDatabase
import Localize_Swift
import Kingfisher

class BranchViewController: UIViewController {
    @IBOutlet var storeImageView: UIImageView!
    @IBOutlet var locationTextView: UITextView!
    @IBOutlet var timeTextView: UITextView!
    @IBOutlet var contactTextView: UITextView!
    @IBOutlet var callButton: UIButton!
    @IBOutlet var mapView: GMSMapView!
    var branch : BranchDB?
    var languageButton = UIButton(type: .custom)

    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "LINDA EXCHANGE"
        self.navigationController?.navigationBar.backItem?.title = ""
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.font: UIFont(name: "Calibri", size: 20)!, NSAttributedStringKey.foregroundColor: UIColor.white]
        
        self.navigationController?.navigationBar.tintColor = UIColor.white
        
        NotificationCenter.default.addObserver(self, selector: #selector(setupBranch), name: NSNotification.Name( LCLLanguageChangeNotification), object: nil)
        
        setupBranch()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupBranch()
        setupLanguageButton()
        NotificationCenter.default.addObserver(self, selector: #selector(setupBranch), name: NSNotification.Name( LCLLanguageChangeNotification), object: nil)
    }
    
    override func viewWillLayoutSubviews() {
        setupNavigationButton()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self)
    }
    
    func setupLanguageButton() {
        if AppState.sharedInstance.currentLanguage == .TH {
            languageButton.isSelected = false
        } else if AppState.sharedInstance.currentLanguage == .EN {
            languageButton.isSelected = true
        }
    }
    
    @objc func setupBranch() {
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineSpacing = 3
        let textColor = UIColor(red: 19 / 255.0, green: 41 / 255.0, blue: 78 / 255.0, alpha: 1.0)
        let font = UIFont(name: "FreesiaUPC", size: 20.0)!
        let attributes = [NSAttributedStringKey.paragraphStyle : paragraphStyle,
                          NSAttributedStringKey.foregroundColor : textColor,
                          NSAttributedStringKey.font : font]
        let fontBold = UIFont(name: "FreesiaUPCBold", size: 22.0)!
        let attributesHead = [NSAttributedStringKey.paragraphStyle : paragraphStyle,
                              NSAttributedStringKey.foregroundColor : textColor,
                              NSAttributedStringKey.font : fontBold]
        
        if let branchData = branch {
            
            self.title = branchData.name[AppState.sharedInstance
                .currentLanguage.rawValue]
            
            // Location Text
            let locationHead = NSAttributedString(string: "Address".localized() + "\n", attributes: attributesHead)
            let locationText = NSAttributedString(string: branchData.address[AppState.sharedInstance
                .currentLanguage.rawValue], attributes: attributes)
            let locationMutable = NSMutableAttributedString(attributedString: locationHead)
            locationMutable.append(locationText)
            
            // Opening Time Text
            let timeHead = NSAttributedString(string: "Opening Time".localized() + "\n", attributes: attributesHead)
            let timeText = NSAttributedString(string: branchData.openTime[AppState.sharedInstance
                .currentLanguage.rawValue], attributes: attributes)
            let timeMutable = NSMutableAttributedString(attributedString: timeHead)
            timeMutable.append(timeText)
            
            // Contact Text
            let contactHead = NSAttributedString(string: "Contact Number".localized() + "\n", attributes: attributesHead)
            let contactText = NSAttributedString(string: branchData.contact[AppState.sharedInstance
                .currentLanguage.rawValue], attributes: attributes)
            let contactMutable = NSMutableAttributedString(attributedString: contactHead)
            contactMutable.append(contactText)
            
            
            self.locationTextView.attributedText = locationMutable
            self.timeTextView.attributedText = timeMutable
            self.contactTextView.attributedText = contactMutable
            
            if let checkedUrl = URL(string: branchData.photo[AppState.sharedInstance.currentLanguage.rawValue]) {
                self.downloadImage(url: checkedUrl, imageView: self.storeImageView)
            }
            
            mapView.camera = GMSCameraPosition.camera(withLatitude: branchData.lat[AppState.sharedInstance
                .currentLanguage.rawValue], longitude: branchData.long[AppState.sharedInstance
                    .currentLanguage.rawValue], zoom: 15.0)
            
            let marker = GMSMarker()
            marker.position = CLLocationCoordinate2D(latitude: branchData.lat[AppState.sharedInstance
                .currentLanguage.rawValue], longitude: branchData.long[AppState.sharedInstance
                    .currentLanguage.rawValue])
            marker.title = branchData.name[AppState.sharedInstance
                .currentLanguage.rawValue]
            marker.map = mapView
        }
        let callText = "  Call Now".localized()
        callButton.setTitle(callText, for: .normal)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func makeBranchCall(_ sender: Any) {
        PhoneCallDelegate.phoneCall(self.contactTextView.text)
    }
    
    @objc func changeLanguage(_ button : UIButton!) {
        if button.isSelected {          // EN -> TH
            button.isSelected = false
            AppState.sharedInstance.currentLanguage = .TH
            Localize.setCurrentLanguage("th")
        } else {                        // TH -> EN
            button.isSelected = true
            AppState.sharedInstance.currentLanguage = .EN
            Localize.setCurrentLanguage("en")
        }
        UserDefaults.standard.set(AppState.sharedInstance.currentLanguage.rawValue, forKey: "language")
    }
    
    func downloadImage(url: URL, imageView: UIImageView) {
        imageView.kf.setImage(with: url)
    }
    
    func setupNavigationButton() {
        languageButton.sizeToFit()
        languageButton.setImage(UIImage(named: "bt_language_th"), for: .normal)
        languageButton.setImage(UIImage(named: "bt_language_en"), for: .selected)
        
        languageButton.addTarget(self, action: #selector(changeLanguage(_:)), for: .touchUpInside)
        languageButton.imageView?.contentMode = .scaleAspectFit
        
        setupLanguageButton();
        let languageItem = UIBarButtonItem(customView: languageButton)
        self.navigationItem.setRightBarButtonItems([languageItem], animated: false)
    }
}
