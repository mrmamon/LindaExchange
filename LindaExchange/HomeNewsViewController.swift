//
//  HomeNewsViewController.swift
//  LindaExchange
//
//  Created by Milion Sun on 10/28/2560 BE.
//  Copyright © 2560 LindaExchange. All rights reserved.
//

import UIKit

class HomeNewsViewController: UIViewController {
    @IBOutlet var newsImageView: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    func setImage(_ url: URL) {
        if let checkedUrl = URL(string: newNews.photo[0]) {
            downloadImage(url: url, imageView: self.newsImageView)
        }
    }
    
    func getDataFromUrl(url: URL, completion: @escaping(_ data: Data?, _ response: URLResponse?, _ error: Error?) -> Void) {
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            completion(data, response, error)
            }.resume()
    }
    
    func downloadImage(url: URL, imageView: UIImageView) {
        getDataFromUrl(url: url) { (data, response, error) in
            guard let data = data, error == nil else {
                print(error.debugDescription)
                return
            }
            DispatchQueue.main.async { () -> Void in
                imageView.image = UIImage(data: data)
            }
        }
    }
}
