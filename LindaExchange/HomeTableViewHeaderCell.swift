//
//  HomeTableViewHeaderCell.swift
//  LindaExchange
//
//  Created by Milion Sun on 10/6/2560 BE.
//  Copyright © 2560 LindaExchange. All rights reserved.
//

import UIKit

class HomeTableViewHeaderCell: UITableViewCell {
    @IBOutlet var timeStackView: UIStackView!
    @IBOutlet var headerStackView: UIStackView!
    @IBOutlet var dateTextView: UILabel!
    @IBOutlet var timeTextView: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
}
