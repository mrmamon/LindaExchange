//
//  ConvertMultipleSellTableViewController.swift
//  LindaExchange
//
//  Created by Milion Sun on 10/30/2560 BE.
//  Copyright © 2560 LindaExchange. All rights reserved.
//

import UIKit
import Localize_Swift

class ConvertMultipleSellTableViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate, SelectTableDelegate, ConvertCellDelegate {
    var denominationArray : [DenominationDB] = []
//    var rightDenominationIndex = 0
    var leftDenominationIndexArray : [Bool] = []
    var newDenominationIndexArray : [Bool] = []
    var leftAmountArray : [String] = []
    var rightAmountArray : [Double] = []
    var currentBranch = 0
    var isBuy = true
    var currentRow = 0
//    var total : Double = 0.0
    @IBOutlet var tableView: UITableView!
    @IBOutlet var addButton: UIView!
    @IBOutlet var totalLabel: UILabel!
    @IBOutlet var totalView: UIView!
//    @IBOutlet var buyButton: UIButton!
//    @IBOutlet var sellButton: UIButton!
    @IBOutlet var totalTextLabel: UILabel!
    @IBOutlet var addLabel: UILabel!
    var keyboardMove : CGFloat = 0.0
    var shouldMove = true
    var total : Double = 0.0
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        view.addGestureRecognizer(tap)
        
        let tap2 = UITapGestureRecognizer(target: self, action: #selector(addMore))
        addButton.addGestureRecognizer(tap2)
        
//        buyButton.setTitleColor(.white, for: .selected)
//        buyButton.setBackgroundColor(color: UIColor.init(red: 53.0 / 255.0, green: 92.0 / 255.0, blue: 141.0 / 255.0, alpha: 1.0), forState: .selected)
//        sellButton.setTitleColor(.white, for: .selected)
//        sellButton.setBackgroundColor(color: UIColor.init(red: 53.0 / 255.0, green: 92.0 / 255.0, blue: 141.0 / 255.0, alpha: 1.0), forState: .selected)
//
//        buyButton.isSelected = true
        
//        addButton.layer.borderColor = UIColor(red: 148.0 / 255.0, green: 148.0 / 255.0, blue: 148.0 / 255.0, alpha: 1).cgColor
//        addButton.layer.borderWidth = 1
        totalView.layer.borderColor = UIColor(red: 148.0 / 255.0, green: 148.0 / 255.0, blue: 148.0 / 255.0, alpha: 1).cgColor
        totalView.layer.borderWidth = 1
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setText()
        NotificationCenter.default.addObserver(self, selector: #selector(setText), name: NSNotification.Name( LCLLanguageChangeNotification), object: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return leftDenominationIndexArray.filter{$0 == true}.count
//        return leftDenominationIndexArray.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "multipleSell", for: indexPath) as! ConvertMultipleSellTableViewCell

        // Configure the cell...
        let row = indexPath.row
        let array = leftDenominationIndexArray.enumerated().filter{$0.element == true}.map{$0.offset}
        
        cell.delegate = self
        if array.count > row {
            let leftDenomination = denominationArray[array[row]]
            cell.leftCurrencyTextField.text = leftDenomination.currencyName + " " + leftDenomination.denominationName
//            cell.leftCurrencyLabel.text = leftDenomination.currencyName
            if isBuy {
                cell.rateDetailLabel.text = "1 " + leftDenomination.currencyName + " : " + leftDenomination.buy[currentBranch] + " THB"
            } else {
                cell.rateDetailLabel.text = "1 " + leftDenomination.currencyName + " : " + leftDenomination.sell[currentBranch] + " THB"
            }
            var rate : Double = 0.0
            let formatter = NumberFormatter()
            formatter.numberStyle = .none
            if isBuy, let buyRate = formatter.number(from: leftDenomination.buy[currentBranch]) {
                rate = buyRate.doubleValue
            } else if !isBuy, let sellRate = formatter.number(from: leftDenomination.sell[currentBranch]) {
                rate = sellRate.doubleValue
            }
            cell.rate = rate
            cell.leftAmountTextField.text = leftAmountArray[array[row]]
            
            if rightAmountArray.count > array[row] {
                cell.rightAmountLabel.text = String.init(format: "%.2f", rightAmountArray[array[row]])
//                cell.rightAmountLabel.text = String.init(format: "%.2f THB", rightAmountArray[array[row]])
            }
        }
//        print("\(leftAmountArray)")
        cell.rightCurrencyTextField.text = "THB"
        cell.rightCurrencyTextField.isEnabled = false
        cell.leftCurrencyTextField.delegate = self
        
        cell.index = array[row]

        return cell
    }

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
//        hidePickerView(true)
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        let row = textField.tag
        self.currentRow = row
//        hidePickerView(false)
        return false
    }

    func setDenominationArray(_ array : [DenominationDB]) {
        var newLeftDominationArray : [Bool] = []
        var newLeftAmountArray : [String] = []
        var newRightAmountArray : [Double] = []
        for i in 0..<array.count {
            if self.denominationArray.count > i
                && self.denominationArray[i].currencyName == array[i].currencyName
                && self.denominationArray[i].denominationName == array[i].denominationName {
                
                newLeftDominationArray.append(self.leftDenominationIndexArray[i])
                newLeftAmountArray.append(self.leftAmountArray[i])
            } else {
                newLeftDominationArray.append(false)
                newLeftAmountArray.append("0")
            }
            newRightAmountArray.append(0.0)
        }
        self.leftDenominationIndexArray = newLeftDominationArray
        self.leftAmountArray = newLeftAmountArray
        self.rightAmountArray = newRightAmountArray
        self.denominationArray = array
        self.recalculate()
        self.changeUI()
    }
    
    func setCurrentBranchIndex(_ index : Int) {
        self.currentBranch = index
        self.recalculate()
        self.changeUI()
    }
    
//    @IBAction func changeToBuy(_ sender: Any) {
//        if !(buyButton.isSelected) {
//            buyButton.isSelected = true
//            sellButton.isSelected = false
//            setIsBuyValue(true)
//        }
//    }
//
//    @IBAction func changeToSell(_ sender: Any) {
//        if buyButton.isSelected {
//            buyButton.isSelected = false
//            sellButton.isSelected = true
//            setIsBuyValue(false)
//        }
//    }
    
    func setIsBuyValue(_ isBuy : Bool) {
        self.isBuy = isBuy
        self.recalculate()
        self.changeUI()
    }
    
    func changeUI() {
        if let _ = tableView {
            DispatchQueue.main.async {
                self.tableView.reloadData()
//                self.tableView.setNeedsLayout()
//                self.tableView.setNeedsDisplay()
            }
        }
    }
    
    func recalculate() {
//        print("\(leftAmountArray)")
        for i in 0..<leftAmountArray.count {
            let formatter = NumberFormatter()
            formatter.numberStyle = .none
            if let left = formatter.number(from: leftAmountArray[i]) {
                var rate : Double = 0.0
                if isBuy {
                    if let buyRate = formatter.number(from: denominationArray[i].buy[currentBranch]) {
                        rate = buyRate.doubleValue
                    }
                } else {
                    if let sellRate = formatter.number(from: denominationArray[i].sell[currentBranch]) {
                        rate = sellRate.doubleValue
                    }
                }
                rightAmountArray[i] = left.doubleValue * rate
            }
        }
//        print("\(leftAmountArray)")
        changeTotalLabel()
    }
    
    @objc func addMore() {
        self.newDenominationIndexArray = self.leftDenominationIndexArray
        let alert = UIAlertController(title: "Set Currencies".localized(), message: "", preferredStyle: .alert)
        let cancelAction = UIAlertAction(title: "Cancel".localized(), style: .destructive) { (action) in
            // Clear Temporary Array
            self.newDenominationIndexArray = self.leftDenominationIndexArray
        }
        let okAction = UIAlertAction(title: "Save".localized(), style: .default) { (action) in
            let duple = zip(self.leftDenominationIndexArray, self.newDenominationIndexArray)
            var index = 0
            var allIndex = 0
            self.tableView.beginUpdates()
            for (old, new) in duple {
                if old && new {
                    index += 1
                } else if old && !new {                         // Delete Row
                    self.tableView.deleteRows(at: [IndexPath(row: index, section: 0)], with: .automatic)
                    self.leftAmountArray[allIndex] = "0"
                    self.rightAmountArray[allIndex] = 0.0
                } else if !old && new {                         // Insert Row
                    self.tableView.insertRows(at: [IndexPath(row: index, section: 0)], with: .automatic)
                    index += 1
                }
                allIndex += 1
            }
            self.leftDenominationIndexArray = self.newDenominationIndexArray
            self.tableView.endUpdates()
        }
        
        alert.addAction(cancelAction)
        alert.addAction(okAction)
        
        let storyBoard = UIStoryboard(name: "Main", bundle: Bundle.main)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "ConvertSelect") as! ConvertSelectTableViewController
        viewController.denominationArray = denominationArray
        viewController.selectedDenomination = leftDenominationIndexArray
        viewController.preferredContentSize = CGSize(width: 272, height: 176)
        viewController.delegate = self
        alert.setValue(viewController, forKey: "contentViewController")
        present(alert, animated: true)
    }
    
    // Called when CheckBox is touched
    func finishSelection(_ array: [Bool]) {
        self.newDenominationIndexArray = array
//        let oldArray = leftDenominationIndexArray
//        leftDenominationIndexArray = array
//        let duple = zip(oldArray, array)
//        var index = 0
//        var allIndex = 0
//        tableView.beginUpdates()
//        for (old, new) in duple {
//            if old && new {
//                index += 1
//            } else if old && !new {                         // Delete Row
//                tableView.deleteRows(at: [IndexPath(row: index, section: 0)], with: .automatic)
//                leftAmountArray[allIndex] = "0"
//                rightAmountArray[allIndex] = 0.0
//            } else if !old && new {                         // Insert Row
//                tableView.insertRows(at: [IndexPath(row: index, section: 0)], with: .automatic)
//                index += 1
//            }
//            allIndex += 1
//        }
//        tableView.endUpdates()
    }
    
    // Called when Left TextView in Cell is changed
    func moneyChanged(index: Int, left: String?, right: Double) {
        if rightAmountArray.count > index {
            rightAmountArray[index] = right
        }
        if leftAmountArray.count > index {
            if let text = left {
                leftAmountArray[index] = text
            } else {
                leftAmountArray[index] = "0"
            }
        }
        changeTotalLabel()
    }
    
    func changeTotalLabel() {
        // Sum all right amount
        total = rightAmountArray.reduce(0) { $0 + $1 }
        if let label = totalLabel {
            totalTextLabel.text = "Total".localized()
            
            let formatter = NumberFormatter()
            formatter.numberStyle = .decimal
            formatter.usesGroupingSeparator = true
//            formatter.locale = Locale.current
//            formatter.groupingSeparator = ","
            formatter.maximumFractionDigits = 2
            formatter.minimumFractionDigits = 2
            if let num = formatter.string(from: NSNumber(value: abs(total))) {
                label.text = num + " THB".localized()
            } else {
                label.text = String.init(format: "%.2f " + "THB".localized(), abs(total))
            }
        }
    }
    
    @objc func keyboardWillShow(notification : NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            if keyboardMove == 0.0 && shouldMove {
                keyboardMove = totalView.frame.origin.y
                self.parent!.view.frame.origin.y -= keyboardSize.height - self.tabBarController!.tabBar.frame.size.height - self.addButton.frame.height
//                totalView.frame.origin.y = self.view.frame.height - keyboardSize.height
//                print("Show \(keyboardSize) \(totalView.frame.origin.y)")
            }
        }
    }
    
    @objc func keyboardWillHide(notification : NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            if keyboardMove != 0.0 {
                self.parent!.view.frame.origin.y += keyboardSize.height - self.tabBarController!.tabBar.frame.size.height - self.addButton.frame.height
                
//                totalView.frame.origin.y = keyboardMove
                keyboardMove = 0.0
//                print("Hide \(keyboardSize) \(totalView.frame.origin.y)")
            }
        }
    }
    
    func moveUp() {
        shouldMove = true
    }
    
    func moveDown() {
        shouldMove = false
    }
    
    @objc func setText() {
        
        totalTextLabel.text = "Total".localized()
        let formatter = NumberFormatter()
        formatter.numberStyle = .decimal
        formatter.usesGroupingSeparator = true
        formatter.locale = Locale.current
        formatter.groupingSeparator = ","
        formatter.maximumFractionDigits = 2
        formatter.minimumFractionDigits = 2
        if let num = formatter.string(from: NSNumber(value: abs(total))) {
            totalLabel.text = num + " THB".localized()
        } else {
            totalLabel.text = String.init(format: "%.2f " + "THB".localized(), abs(total))
        }
        addLabel.text = "+ Add Currencies".localized()
    }
    
    func addKeyboardObserver() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(notification:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(notification:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    func removeKeyboardObserver() {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
}
