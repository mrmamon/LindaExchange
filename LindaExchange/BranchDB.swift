//
//  BranchDB.swift
//  LindaExchange
//
//  Created by Milion Sun on 9/20/2560 BE.
//  Copyright © 2560 LindaExchange. All rights reserved.
//

import Foundation
import FirebaseDatabase

struct BranchDB {
    let key: String
    var address: [String] = []
    var contact: [String] = []
    var lat: [Double] = []
    var long: [Double] = []
    var name: [String] = []
    var openTime: [String] = []
    var photo: [String] = []
    
    init(_ snapshot: DataSnapshot) {
        key = snapshot.key
        let snapshotArray = snapshot.value as! [[String : String]]
        for snapshotValue in snapshotArray {
        
            if let address = snapshotValue["address"] {
                self.address.append(address)
            } else {
                self.address.append("")
            }
            
            if let contact = snapshotValue["contactnumber"] {
                self.contact.append(contact)
            } else {
                self.contact.append("")
            }
            
            if let name = snapshotValue["name"] {
                self.name.append(name)
            } else {
                self.name.append("")
            }
            
            if let openTime = snapshotValue["openingtime"] {
                self.openTime.append(openTime)
            } else {
                self.openTime.append("")
            }
        
            if let location = snapshotValue["location"] {
                let locationArray = location.components(separatedBy: ",")
                if locationArray.count == 2 {
                    self.lat.append(Double(locationArray[0])!)
                    self.long.append(Double(locationArray[1])!)
                } else {
                    self.lat.append(0.0)
                    self.long.append(0.0)
                }
            } else {
                self.lat.append(0.0)
                self.long.append(0.0)
            }
            
            if let photo = snapshotValue["photo"] {
                self.photo.append(photo)
            } else {
                self.photo.append("")
            }
        }
    }
}
