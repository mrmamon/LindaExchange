//
//  ConvertSelectTableViewController.swift
//  LindaExchange
//
//  Created by Milion Sun on 10/29/2560 BE.
//  Copyright © 2560 LindaExchange. All rights reserved.
//

import UIKit

protocol SelectTableDelegate: class {
    func finishSelection(_ array : [Bool])
}

class ConvertSelectTableViewController: UITableViewController {
    var denominationArray : [DenominationDB] = []
    var selectedDenomination : [Bool] = []
    weak var delegate : SelectTableDelegate?
    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return denominationArray.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "convertSelect", for: indexPath) as! ConvertSelectTableViewCell
        
        let denomination = denominationArray[indexPath.row]
        cell.denominationLabel.text = denomination.currencyName + " " +  denomination.denominationName
        cell.setFlag(denomination.flag)
        cell.countryLabel.text = denomination.countryName
        cell.convertCheckBox.isChecked = selectedDenomination[indexPath.row]
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let cell = tableView.cellForRow(at: indexPath) as? ConvertSelectTableViewCell {
            if cell.convertCheckBox.isChecked {
                cell.convertCheckBox.isChecked = false
                selectedDenomination[indexPath.row] = false
            } else {
                cell.convertCheckBox.isChecked = true
                selectedDenomination[indexPath.row] = true
            }
        }
        if let delegate = self.delegate {
            delegate.finishSelection(selectedDenomination)
        }
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 40
    }

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    func setSelectedIndex(_ array : [Bool]) {
        self.selectedDenomination = array
    }
    
    func getSelectedIndex() -> [Bool] {
        return self.selectedDenomination
    }
}
