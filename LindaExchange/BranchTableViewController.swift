//
//  BranchTableViewController.swift
//  LindaExchange
//
//  Created by Milion Sun on 8/30/2560 BE.
//  Copyright © 2560 LindaExchange. All rights reserved.
//

import UIKit
import FirebaseDatabase
import Localize_Swift

class BranchTableViewController: UITableViewController {
    var languageButton = UIButton(type: .custom)
    var branchArray : [BranchDB] = []
    let alert = UIAlertController(title: nil, message: "Please Wait...", preferredStyle: .alert)

    override func viewDidLoad() {
        super.viewDidLoad()
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
        
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.font: UIFont(name: "Calibri", size: 20)!, NSAttributedStringKey.foregroundColor: UIColor.white]
        
        NotificationCenter.default.addObserver(self, selector: #selector(setText), name: NSNotification.Name( LCLLanguageChangeNotification), object: nil)
        
        present(alert, animated: true, completion: nil)
        let ref = Database.database().reference()
        
        ref.child("branch").observe(.value, with: { (snapshot) in
            self.branchArray.removeAll()
            for item in snapshot.children {
                let newBranch = BranchDB(item as! DataSnapshot)
                self.branchArray.append(newBranch)
                print(newBranch.key)
            }
            self.tableView.reloadData()
            self.alert.dismiss(animated: true, completion: nil)
        })
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setText()
        setupLanguageButton()
        NotificationCenter.default.addObserver(self, selector: #selector(setText), name: NSNotification.Name( LCLLanguageChangeNotification), object: nil)
    }
    
    override func viewWillLayoutSubviews() {
        setupNavigationButton()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self)
    }
    
    func setupLanguageButton() {
        if AppState.sharedInstance.currentLanguage == .TH {
            languageButton.isSelected = false
        } else if AppState.sharedInstance.currentLanguage == .EN {
            languageButton.isSelected = true
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return branchArray.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "branchCell", for: indexPath) as! BranchTableViewCell

        // Configure the cell...
        cell.nameLabel.text = branchArray[indexPath.row].name[AppState.sharedInstance.currentLanguage.rawValue]
        
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineSpacing = 3
        let textColor = UIColor(red: 19 / 255.0, green: 41 / 255.0, blue: 78 / 255.0, alpha: 1.0)
        let font = UIFont(name: "FreesiaUPC", size: 25.0)!
        let attributes = [NSAttributedStringKey.paragraphStyle : paragraphStyle,
                          NSAttributedStringKey.foregroundColor : textColor,
                          NSAttributedStringKey.font : font]
        cell.timeLabel.attributedText = NSAttributedString(string: branchArray[indexPath.row].openTime[AppState.sharedInstance.currentLanguage.rawValue], attributes: attributes)

        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let row = indexPath.row
        let branchDetail = self.storyboard?.instantiateViewController(withIdentifier: "BranchDetail") as! BranchViewController
        branchDetail.branch = branchArray[row]
        self.navigationController?.pushViewController(branchDetail, animated: true)
    }

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    @objc func changeLanguage(_ button : UIButton!) {
        if button.isSelected {          // EN -> TH
            button.isSelected = false
            AppState.sharedInstance.currentLanguage = .TH
            Localize.setCurrentLanguage("th")
        } else {                        // TH -> EN
            button.isSelected = true
            AppState.sharedInstance.currentLanguage = .EN
            Localize.setCurrentLanguage("en")
        }
        UserDefaults.standard.set(AppState.sharedInstance.currentLanguage.rawValue, forKey: "language")
    }
    
    @objc func setText() {
        tableView.reloadData()
        self.navigationItem.title = "Branches".localized()
    }
    
    func setupNavigationButton() {
        let logo = UIButton(type: .custom)
        let logoImage = UIImage(named: "logo_linda")
        logo.sizeToFit()
        
        languageButton.sizeToFit()
        languageButton.setImage(UIImage(named: "bt_language_th"), for: .normal)
        languageButton.setImage(UIImage(named: "bt_language_en"), for: .selected)
        
        languageButton.addTarget(self, action: #selector(changeLanguage(_:)), for: .touchUpInside)
        languageButton.imageView?.contentMode = .scaleAspectFit
        
        setupLanguageButton();
        let languageItem = UIBarButtonItem(customView: languageButton)
        
        logo.setImage(logoImage, for: .normal)
        logo.imageView?.contentMode = .scaleAspectFit
        let logoItem = UIBarButtonItem(customView: logo)
        self.navigationItem.setLeftBarButtonItems([logoItem], animated: false)
        self.navigationItem.setRightBarButtonItems([languageItem], animated: false)
    }
    
    func resizeImageWithHeight(image : UIImage?, height : CGFloat) -> UIImage? {
        if let image = image {
            let size = CGSize(width: image.size.width * height / image.size.height, height: height)
            UIGraphicsBeginImageContextWithOptions(size, false, 1.0)
            image.draw(in: CGRect(x: 0, y: 0, width: size.width, height: size.height))
            let newImage = UIGraphicsGetImageFromCurrentImageContext()
            UIGraphicsEndImageContext()
            return newImage
        }
        return nil
    }
}
