//
//  NewsTableViewController.swift
//  LindaExchange
//
//  Created by Milion Sun on 8/30/2560 BE.
//  Copyright © 2560 LindaExchange. All rights reserved.
//

import UIKit
import FirebaseDatabase
import Localize_Swift

class NewsTableViewController: UITableViewController {
    var newsArray : [NewsDB] = []
    var languageButton = UIButton(type: .custom)
//    @IBOutlet var tableView: UITableView!
    let alert = UIAlertController(title: nil, message: "Please Wait...", preferredStyle: .alert)

    override func viewDidLoad() {
        super.viewDidLoad()
//        self.title = "LINDA EXCHANGE"
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
        
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.font: UIFont(name: "Calibri", size: 20)!, NSAttributedStringKey.foregroundColor: UIColor.white]
        
        NotificationCenter.default.addObserver(self, selector: #selector(setText), name: NSNotification.Name( LCLLanguageChangeNotification), object: nil)
        
        present(alert, animated: true, completion: nil)
        let ref = Database.database().reference()
        ref.child("news").queryOrdered(byChild: "epoch").observe(.value, with: { (snapshot) in
            self.newsArray.removeAll()
            for item in snapshot.children {
                let newNews = NewsDB(item as! DataSnapshot)
                self.newsArray.append(newNews)
            }
            self.newsArray.reverse()
            self.tableView.reloadData()
            self.alert.dismiss(animated: true, completion: nil)
        })
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setText()
        setupLanguageButton()
        NotificationCenter.default.addObserver(self, selector: #selector(setText), name: NSNotification.Name( LCLLanguageChangeNotification), object: nil)
    }
    
    override func viewWillLayoutSubviews() {
        setupNavigationButton()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self)
    }
    
    func setupLanguageButton() {
        if AppState.sharedInstance.currentLanguage == .TH {
            languageButton.isSelected = false
        } else if AppState.sharedInstance.currentLanguage == .EN {
            languageButton.isSelected = true
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return newsArray.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "newsCell", for: indexPath) as! NewsTableViewCell

        // Configure the cell...
        cell.titleLabel.text = newsArray[indexPath.row].topic[AppState.sharedInstance.currentLanguage.rawValue]
        
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineSpacing = 3
        let textColor = UIColor(red: 19 / 255.0, green: 41 / 255.0, blue: 78 / 255.0, alpha: 1.0)
//        let font = UIFont(name: "FreesiaUPC", size: 23.0)!
        let font = UIFont.systemFont(ofSize: 15)
        let attributes = [NSAttributedStringKey.paragraphStyle : paragraphStyle,
                          NSAttributedStringKey.foregroundColor : textColor,
                          NSAttributedStringKey.font : font]
        let detailText = NSAttributedString(string: newsArray[indexPath.row].detail[AppState.sharedInstance.currentLanguage.rawValue], attributes: attributes)
        cell.detailLabel.attributedText = detailText
        
        var url = newsArray[indexPath.row].photo[AppState.sharedInstance.currentLanguage.rawValue]
        if !isEscaped(url) {
            url = url.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
        }
        if let checkedUrl = URL(string: url) {
//            cell.newsImage.contentMode = .scaleAspectFill
            downloadImage(url: checkedUrl, imageView: cell.newsImage)
        }

        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let news = newsArray[indexPath.row]
        let newsDetail = self.storyboard?.instantiateViewController(withIdentifier: "NewsDetail") as! NewsViewController
        newsDetail.news = news
        self.navigationController?.pushViewController(newsDetail, animated: true)
    }
    
    @IBAction func makePhoneCall(_ sender: Any) {
        PhoneCallDelegate.phoneCall()
    }
    
    func downloadImage(url: URL, imageView: UIImageView) {
        imageView.kf.setImage(with: url)
    }
    
    @objc func changeLanguage(_ button : UIButton!) {
        if button.isSelected {          // EN -> TH
            button.isSelected = false
            AppState.sharedInstance.currentLanguage = .TH
            Localize.setCurrentLanguage("th")
        } else {                        // TH -> EN
            button.isSelected = true
            AppState.sharedInstance.currentLanguage = .EN
            Localize.setCurrentLanguage("en")
        }
        UserDefaults.standard.set(AppState.sharedInstance.currentLanguage.rawValue, forKey: "language")
    }
    
    @objc func setText() {
        tableView.reloadData()
        self.navigationItem.title = "News".localized()
    }
    
    func setupNavigationButton() {
        let logo = UIButton(type: .custom)
        let logoImage = UIImage(named: "logo_linda")
        logo.sizeToFit()
        
        languageButton.sizeToFit()
        languageButton.setImage(UIImage(named: "bt_language_th"), for: .normal)
        languageButton.setImage(UIImage(named: "bt_language_en"), for: .selected)
        
        languageButton.addTarget(self, action: #selector(changeLanguage(_:)), for: .touchUpInside)
        languageButton.imageView?.contentMode = .scaleAspectFit
        
        setupLanguageButton();
        let languageItem = UIBarButtonItem(customView: languageButton)
        
        logo.setImage(logoImage, for: .normal)
        logo.imageView?.contentMode = .scaleAspectFit
        let logoItem = UIBarButtonItem(customView: logo)
        self.navigationItem.setLeftBarButtonItems([logoItem], animated: false)
        self.navigationItem.setRightBarButtonItems([languageItem], animated: false)
    }
    
    func isEscaped(_ url: String) -> Bool {
        return url != url.removingPercentEncoding
    }
}
