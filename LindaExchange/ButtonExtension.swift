//
//  buttonExtension.swift
//  LindaExchange
//
//  Created by Milion Sun on 11/1/2560 BE.
//  Copyright © 2560 LindaExchange. All rights reserved.
//

import UIKit

extension UIButton {
    func setBackgroundColor(color : UIColor, forState : UIControlState) {
        UIGraphicsBeginImageContext(CGSize(width: 1, height: 1))
        if let ctx = UIGraphicsGetCurrentContext() {
            let rect = CGRect(x: 0, y: 0, width: 1, height: 1)
            ctx.setFillColor(color.cgColor)
            ctx.fill(rect)
            if let image = UIGraphicsGetImageFromCurrentImageContext() {
                self.setBackgroundImage(image, for: .selected)
            }
        }
        UIGraphicsEndImageContext()
    }
}
