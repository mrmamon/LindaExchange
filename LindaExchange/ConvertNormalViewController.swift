//
//  ConvertNormalViewController.swift
//  LindaExchange
//
//  Created by Milion Sun on 10/30/2560 BE.
//  Copyright © 2560 LindaExchange. All rights reserved.
//

import UIKit
import Localize_Swift

class ConvertNormalViewController: UIViewController, UITextFieldDelegate, UIPickerViewDelegate, UIPickerViewDataSource {
    @IBOutlet var leftCurrencyTextField: UITextField!
    @IBOutlet var rightCurrencyTextField: UITextField!
    @IBOutlet var leftAmountTextField: UITextField!
    @IBOutlet var rightAmountLabel: UILabel!
//    @IBOutlet var leftCurrencyLabel: UILabel!
    @IBOutlet var rateDetailLabel: UILabel!
    @IBOutlet var buyButton: UIButton!
    @IBOutlet var sellButton: UIButton!
    @IBOutlet var buyExplainLabel: UILabel!
    @IBOutlet var sellExplainLabel: UILabel!
    var rateArray : [DenominationDB] = []
//    var currentCurrencyTextField : UITextField? = nil
    let currencyPickerView = UIPickerView()
    
    var isLeft = true
    var isBuy = true
    var currentBranch = 0
    var pickerIndex = 0
    var currentIndex = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        view.addGestureRecognizer(tap)
        leftAmountTextField.addTarget(self, action: #selector(leftAmountChange(_:)), for: UIControlEvents.editingChanged)
        
        buyButton.setTitleColor(.white, for: .selected)
        buyButton.contentEdgeInsets = UIEdgeInsets(top: -30, left: 0, bottom: -30, right: 0)
        buyButton.setBackgroundColor(color: UIColor.init(red: 53.0 / 255.0, green: 92.0 / 255.0, blue: 141.0 / 255.0, alpha: 1.0), forState: .selected)
        sellButton.setTitleColor(.white, for: .selected)
        sellButton.setBackgroundColor(color: UIColor.init(red: 53.0 / 255.0, green: 92.0 / 255.0, blue: 141.0 / 255.0, alpha: 1.0), forState: .selected)
        
        buyButton.isSelected = true
        
//        currencyPickerView = UIPickerView()
        currencyPickerView.delegate = self
        currencyPickerView.dataSource = self
        rightCurrencyTextField.inputView = currencyPickerView
        leftCurrencyTextField.inputView = currencyPickerView
        
        let toolbar = UIToolbar()
        toolbar.barStyle = .default
        toolbar.isTranslucent = true
        toolbar.sizeToFit()
        
        let doneButton = UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(pickerDone(_:)))
        let space = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(pickerCancel(_:)))
        toolbar.setItems([cancelButton, space, doneButton], animated: false)
        toolbar.isUserInteractionEnabled = true
        rightCurrencyTextField.inputAccessoryView = toolbar
        leftCurrencyTextField.inputAccessoryView = toolbar
        
        currencyPickerView.reloadAllComponents()
        
        rightCurrencyTextField.rightViewMode = .never
        let dropdownImage = UIImageView(image: UIImage(named: "expand-button"))
        if let size = dropdownImage.image?.size {
            dropdownImage.frame = CGRect(x: 0.0, y: 0.0, width: size.width + 12.0, height: size.height)
        }
        dropdownImage.contentMode = .center
        rightCurrencyTextField.rightView = dropdownImage
        
        leftCurrencyTextField.rightViewMode = .always
        let dropdownImage1 = UIImageView(image: UIImage(named: "expand-button"))
        if let size = dropdownImage1.image?.size {
            dropdownImage1.frame = CGRect(x: 0.0, y: 0.0, width: size.width + 12.0, height: size.height)
        }
        dropdownImage1.contentMode = .center
        leftCurrencyTextField.rightView = dropdownImage1
        
        changeUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setText()
        NotificationCenter.default.addObserver(self, selector: #selector(setText), name: NSNotification.Name( LCLLanguageChangeNotification), object: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return rateArray.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return rateArray[row].currencyName + " " + rateArray[row].denominationName
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        pickerIndex = row
    }
    
    func changeCurrencyTextField(_ textField : UITextField) {
        if rateArray.count > pickerIndex {
            textField.text = rateArray[pickerIndex].currencyName + " " + rateArray[pickerIndex].denominationName
        } else {
            textField.text = ""
        }
    }
    
    func changeRateDetail() {
        var text = ""
        text += translateRateDetail()
        self.rateDetailLabel.text = text
    }
    
    func translateRateDetail() -> String {
        var text = ""
        if rateArray.count > currentIndex {
            text += "1 " + rateArray[currentIndex].currencyName + " : "
            if isBuy {
                text += rateArray[currentIndex].buy[currentBranch] + " "
            } else {
                text += rateArray[currentIndex].sell[currentBranch] + " "
            }
            text += "THB"
        }
        return text
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
        hidePicker(true)
    }

    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == leftAmountTextField {
            if let text = textField.text {
                if let decimalIndex = text.index(of: ".") {
                    if (string == "0" || string.isEmpty) && decimalIndex.encodedOffset < range.location {
                        return true
                    }
                    
                    if string == "." {
                        return false
                    }
                }
                
                let formatter = NumberFormatter()
                formatter.numberStyle = .decimal
                formatter.usesGroupingSeparator = true
                formatter.groupingSeparator = ","
                formatter.maximumIntegerDigits = 18
                formatter.maximumFractionDigits = 30
                formatter.roundingMode = .floor
                var combinedText = text
                if let r = Range<String.Index>(range, in: combinedText) {
                    combinedText.replaceSubrange(r, with: string)
                }
                var textWithoutComma = combinedText.replacingOccurrences(of: ",", with: "")
                if let number = formatter.number(from: textWithoutComma) {
                    if let newString = formatter.string(from: number) {
                        textWithoutComma = newString
                    }
                }
                
                if string == "." && range.location == text.count {
                    textWithoutComma += "."
                }
                if textWithoutComma.isEmpty {
                    textWithoutComma = "0"
                }
                textField.text = textWithoutComma
                leftAmountChange(textField)
            }
        }
        return false
    }
    
    @objc func leftAmountChange(_ sender:UITextField) {
        var currency = "THB"
        var formattedString = "0.00"
        if !isBuy {
            formattedString = "0.000"
            if rateArray.count > currentIndex {
                currency = rateArray[currentIndex].currencyName
            }
        }
        if var text = sender.text {
            text = text.replacingOccurrences(of: ",", with: "")
            let formatter = NumberFormatter()
            formatter.numberStyle = .decimal
            formatter.groupingSeparator = ","
            formatter.usesGroupingSeparator = true
            if let left = formatter.number(from: text) {
                if isBuy {
                    formatter.minimumFractionDigits = 2
                    formatter.maximumFractionDigits = 2
                    if let string = formatter.string(from: NSNumber(value: leftToTHB(left.doubleValue))) {
                        formattedString = string
                    }
                } else {
                    formatter.minimumFractionDigits = 3
                    formatter.maximumFractionDigits = 3
                    if let string = formatter.string(from: NSNumber(value: THBToRight(left.doubleValue))) {
                        formattedString = string
                    }
                }
            }
        }
//        rightAmountLabel.text = formattedString + " \(currency)"
        rightAmountLabel.text = formattedString
    }
    
    func leftToTHB(_ left : Double) -> Double {
        return left * getRate()
    }
    
    func THBToRight(_ thb : Double) -> Double {
        return thb / getRate()
    }
    
    func getRate() -> Double {
        if rateArray.count > currentIndex {
            let rate = rateArray[currentIndex]
            let formatter = NumberFormatter()
            formatter.numberStyle = .none
            if isBuy {
                if let rateNumber = formatter.number(from: rate.buy[currentBranch]) {
                    return rateNumber.doubleValue
                }
            } else {
                if let rateNumber = formatter.number(from: rate.sell[currentBranch]) {
                    return rateNumber.doubleValue
                }
            }
        }
        return 0
    }
    
    @IBAction func changeToBuy(_ sender: Any) {
        if !(buyButton.isSelected) {
            buyButton.isSelected = true
            sellButton.isSelected = false
            setIsBuyValue(true)
        }
    }
    
    @IBAction func changeToSell(_ sender: Any) {
        if buyButton.isSelected {
            buyButton.isSelected = false
            sellButton.isSelected = true
            setIsBuyValue(false)
        }
    }
    
    func setRateArray(_ array : [ExchangeRateDB]) {
        var newArray : [DenominationDB] = []
        for item in array {
            newArray.append(contentsOf: item.getDominationDB())
        }
        self.rateArray = newArray
        changeUI()
    }
    
    func setIsBuyValue(_ isBuy : Bool) {
        self.isBuy = isBuy
        leftAmountTextField.text = "0"
        changeUI()
    }
    
    func setCurrentBranchIndex(_ index : Int) {
        currentBranch = index
        changeUI()
    }
    
    func changeUI() {
        if let right = rightCurrencyTextField, let left = leftCurrencyTextField {
            var currencyName = ""
            var denominationName = ""
            if rateArray.count > currentIndex {
                let rate = rateArray[currentIndex]
                currencyName = rate.currencyName
                denominationName = rate.denominationName
            }
            var text = currencyName
            if denominationName != "" {
                text += " " + denominationName
            }
            if isBuy {
                left.rightViewMode = .always
                right.rightViewMode = .never
                left.text = text
                right.text = "THB"
//                leftCurrencyLabel.text = currencyName
                left.isEnabled = true
                right.isEnabled = false
            } else {
                right.rightViewMode = .always
                left.rightViewMode = .never
                left.text = "THB"
                right.text = text
//                leftCurrencyLabel.text = "THB"
                left.isEnabled = false
                right.isEnabled = true
            }
        }
        if let _ = rateDetailLabel {
            changeRateDetail()
        }
        if let _ = leftAmountTextField {
            leftAmountChange(leftAmountTextField)
        }
    }
    
    func hidePicker(_ hide : Bool) {
        if hide {
            if isBuy {
                leftCurrencyTextField.resignFirstResponder()
            } else {
                rightCurrencyTextField.resignFirstResponder()
            }
        }
//        currencyPickerView.isHidden = hide
    }
    
    @objc func pickerDone(_ sender: Any) {
        hidePicker(true)
        self.currentIndex = self.pickerIndex
        if isBuy {
            changeCurrencyTextField(leftCurrencyTextField)
//            leftCurrencyLabel.text = rateArray[currentIndex].currencyName
        } else {
            changeCurrencyTextField(rightCurrencyTextField)
        }
        changeRateDetail()
        leftAmountChange(leftAmountTextField)
    }
    
    @objc func pickerCancel(_ sender: Any) {
        hidePicker(true)
    }
    
    @objc func setText() {
        let buyText = "Buying Rate".localized()
        buyButton.setTitle(buyText, for: .normal)
        buyButton.setTitle(buyText, for: .selected)
        
        let sellText = "Selling Rate".localized()
        sellButton.setTitle(sellText, for: .normal)
        sellButton.setTitle(sellText, for: .selected)
        
        buyExplainLabel.text = "Foreign Currency\nto Thai Baht".localized()
        sellExplainLabel.text = "Thai Baht to\nForeign Currency".localized()
    }
}
