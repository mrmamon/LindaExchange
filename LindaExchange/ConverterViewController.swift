//
//  ConverterViewController.swift
//  LindaExchange
//
//  Created by Milion Sun on 9/12/2560 BE.
//  Copyright © 2560 LindaExchange. All rights reserved.
//

import UIKit
import FirebaseDatabase
import Localize_Swift

class ConverterViewController: UIViewController, UIPickerViewDataSource, UIPickerViewDelegate, UITextFieldDelegate {
//    @IBOutlet var rightAmountTextField: UITextField!
    var converterPickerView = UIPickerView()
    //    let countryDummy = ["USD", "JPY", "EUR", "GBP", "CHF", "NZD", "AUD", "DKK"]
//    var currentTextField = UITextField()
    @IBOutlet var dateLabel: UILabel!
    @IBOutlet var timeLabel: UILabel!
//    @IBOutlet var branchButton: UIButton!
    @IBOutlet var branchButton: UITextField!
    @IBOutlet var modeButton: UITextField!
    var languageButton = UIButton(type: .custom)
    var rateArray : [ExchangeRateDB] = []
    var branchArray : [BranchDB] = []
    let modeArray = ["Normal", "Multiple", "Exchange"]
    var currentBranch = 0
    var currentMode = 0
    var leftCurrency = 0
    var rightCurrency = 0
    var currentCurrencyTextField : UITextField?
//    var isBranch = false
    let alert = UIAlertController(title: nil, message: "Please Wait...", preferredStyle: .alert)
    var activityCount = 0
    var pickerMode: PickerMode = .denomination
    var pickerRow = 0
    var refreshTime : Date?
    
    @IBOutlet var convertContainerView: UIView!
    
    private lazy var convertNormalViewController : ConvertNormalViewController = {
        let storyBoard = UIStoryboard(name: "Main", bundle: Bundle.main)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "ConvertNormal") as! ConvertNormalViewController
//        viewController.rateArray = self.rateArray
        return viewController
    } ()
    private lazy var convertMultipleSellController : ConvertMultipleSellTableViewController = {
        let storyBoard = UIStoryboard(name: "Main", bundle: Bundle.main)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "MultipleSellViewController") as! ConvertMultipleSellTableViewController
//        self.addChildViewControllerToContainer(viewController)
        return viewController
    } ()
    private lazy var convertTwoCurrencyController : ConvertTwoCurrencyViewController = {
        let storyBoard = UIStoryboard(name: "Main", bundle: Bundle.main)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "ConvertTwoCurrency") as! ConvertTwoCurrencyViewController
        return viewController
    } ()
    var currentViewController : UIViewController? = nil
    
    enum PickerMode {
        case branch, mode, denomination
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.font: UIFont(name: "Calibri", size: 20)!, NSAttributedStringKey.foregroundColor: UIColor.white]
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        view.addGestureRecognizer(tap)
        
        converterPickerView.delegate = self
        converterPickerView.dataSource = self
        branchButton.inputView = converterPickerView
        modeButton.inputView = converterPickerView
        
        let toolbar = UIToolbar()
        toolbar.barStyle = .default
        toolbar.isTranslucent = true
        toolbar.sizeToFit()
        
        let doneButton = UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(pickerDone(_:)))
        let space = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(pickerCancel(_:)))
        toolbar.setItems([cancelButton, space, doneButton], animated: false)
        toolbar.isUserInteractionEnabled = true
        branchButton.inputAccessoryView = toolbar
        modeButton.inputAccessoryView = toolbar
        
        getRateData(self)
        currentViewController = convertNormalViewController
        
        branchButton.rightViewMode = .always
        let dropdownImage1 = UIImageView(image: UIImage(named: "expand-button"))
        if let size = dropdownImage1.image?.size {
            dropdownImage1.frame = CGRect(x: 0.0, y: 0.0, width: size.width + 12.0, height: size.height)
        }
        dropdownImage1.contentMode = .center
        branchButton.rightView = dropdownImage1
        
        modeButton.rightViewMode = .always
        let dropdownImage2 = UIImageView(image: UIImage(named: "expand-button-white"))
        if let size = dropdownImage2.image?.size {
            dropdownImage2.frame = CGRect(x: 0.0, y: 0.0, width: size.width + 12.0, height: size.height)
        }
        dropdownImage2.contentMode = .center
        modeButton.rightView = dropdownImage2
        
        NotificationCenter.default.addObserver(self, selector: #selector(setText), name: NSNotification.Name( LCLLanguageChangeNotification), object: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setText()
        setupLanguageButton()
        NotificationCenter.default.addObserver(self, selector: #selector(setText), name: NSNotification.Name( LCLLanguageChangeNotification), object: nil)
        
        if (currentMode == 1) {
            self.convertMultipleSellController.addKeyboardObserver()
        }
    }
    
    override func viewWillLayoutSubviews() {
        setupNavigationButton()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self)
    }
    
    func setupLanguageButton() {
        if AppState.sharedInstance.currentLanguage == .TH {
            languageButton.isSelected = false
        } else if AppState.sharedInstance.currentLanguage == .EN {
            languageButton.isSelected = true
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if pickerMode == .branch {
            return branchArray.count
        } else if pickerMode == .mode {
            return modeArray.count
        } else {
            return 0
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if pickerMode == .branch {
            return branchArray[row].name[AppState.sharedInstance.currentLanguage.rawValue]
        } else if pickerMode == .mode {
            return modeArray[row].localized()
        } else {
            return ""
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        pickerRow = row
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
        hidePicker(true)
    }
    
    func hidePicker(_ hide : Bool) {
        if let textField = currentCurrencyTextField {
            textField.resignFirstResponder()
        }
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        dismissKeyboard()
        currentCurrencyTextField = textField
        if textField.isEqual(branchButton) {
            if pickerMode != .branch {
                pickerMode = .branch
                converterPickerView.reloadAllComponents()
            }
            converterPickerView.selectRow(currentBranch, inComponent: 0, animated: false)
            pickerRow = currentBranch
        } else if textField.isEqual(modeButton) {
            if pickerMode != .mode {
                pickerMode = .mode
                converterPickerView.reloadAllComponents()
            }
            converterPickerView.selectRow(currentMode, inComponent: 0, animated: false)
            pickerRow = currentMode
        }
//        currentTextField = textField
//        hidePicker(false)
        
        if currentMode == 0 {
            convertNormalViewController.dismissKeyboard()
        } else if currentMode == 1 {
            convertMultipleSellController.dismissKeyboard()
        } else if currentMode == 2 {
            convertTwoCurrencyController.dismissKeyboard()
        }
        return true
    }
    
    @IBAction func getRateData(_ sender: Any) {
        startActivityIndicator()
        let ref = Database.database().reference()
        ref.child("exchangerate").observe(.value, with: { (snapshot) in
            self.rateArray.removeAll()
            for item in snapshot.children {
                let newRate = ExchangeRateDB(item as! DataSnapshot)
                self.rateArray.append(newRate)
            }
            self.converterPickerView.reloadAllComponents()
            if self.currentMode == 0 {
                self.convertNormalViewController.setRateArray(self.rateArray)
                self.removeChildViewControllerFromContainer(self.convertNormalViewController)
                self.addChildViewControllerToContainer(self.convertNormalViewController)
            } else if self.currentMode == 1 {
                var array : [DenominationDB] = []
                for item in self.rateArray {
                    array.append(contentsOf: item.getDominationDB())
                }
                self.convertMultipleSellController.setDenominationArray(array)
                self.removeChildViewControllerFromContainer(self.convertMultipleSellController)
                self.addChildViewControllerToContainer(self.convertMultipleSellController)
            } else if self.currentMode == 2 {
                self.convertTwoCurrencyController.setRateArray(self.rateArray)
                self.removeChildViewControllerFromContainer(self.convertTwoCurrencyController)
                self.addChildViewControllerToContainer(self.convertTwoCurrencyController)
            }
//            self.leftAmountChange(self.leftAmountTextField)
            
            ref.child("updateTime").child("0").observeSingleEvent(of: .value, with: { (timeSnapshot) in
                if let snapshotValue = timeSnapshot.value as? [String : AnyObject] {
                    if let timestamp = snapshotValue["timestamp"] as? Double {
                        let datetime = Date(timeIntervalSince1970: timestamp / 1000.0)
                        self.refreshTime = datetime
                        self.dateLabel.text = self.getDateLabelText(datetime)
                        self.timeLabel.text = self.getTimeLabelText(datetime)
                    }
                } else {
                    let datetime = Date()
                    self.refreshTime = datetime
                    self.dateLabel.text = self.getDateLabelText(datetime)
                    self.timeLabel.text = self.getTimeLabelText(datetime)
                }
                self.stopActivityIndicator()
            })
        })
        
        startActivityIndicator()
        ref.child("branch").observe(.value, with: { (snapshot) in
            self.branchArray.removeAll()
            for item in snapshot.children {
                let newBranch = BranchDB(item as! DataSnapshot)
                self.branchArray.append(newBranch)
                print(newBranch.key)
            }
            if self.branchArray.count > self.currentBranch {
                let branch = self.branchArray[self.currentBranch]
                self.branchButton.text = branch.name[AppState.sharedInstance.currentLanguage.rawValue]
            }
            self.stopActivityIndicator()
        })
    }
    
    func startActivityIndicator() {
        activityCount += 1
        if activityCount == 1 {
            present(alert, animated: true, completion: nil)
        }
    }
    
    func stopActivityIndicator() {
        if activityCount > 0 {
            activityCount -= 1
        }
        if activityCount == 0 {
            alert.dismiss(animated: true, completion: nil)
        }
    }
    
    private func addChildViewControllerToContainer(_ viewController : UIViewController) {
        addChildViewController(viewController)
        convertContainerView.addSubview(viewController.view)
        viewController.view.frame = convertContainerView.bounds
        viewController.view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        viewController.didMove(toParentViewController: self)
    }
    
    private func removeChildViewControllerFromContainer(_ viewController : UIViewController) {
        viewController.willMove(toParentViewController: nil)
        viewController.view.removeFromSuperview()
        viewController.removeFromParentViewController()
    }
    
    @IBAction func pickerDone(_ sender: Any) {
        hidePicker(true)
        let row = pickerRow
        if pickerMode == .branch {
            branchButton.text = branchArray[row].name[AppState.sharedInstance.currentLanguage.rawValue]
            currentBranch = row
            if currentMode == 0 {
                convertNormalViewController.setCurrentBranchIndex(currentBranch)
            } else if currentMode == 1 {
                convertMultipleSellController.setCurrentBranchIndex(currentBranch)
            } else if currentMode == 2 {
                convertTwoCurrencyController.setCurrentBranchIndex(currentBranch)
            }
        } else if pickerMode == .mode {
            if currentMode == row {
                return
            }
            modeButton.text = modeArray[row].localized()
            currentMode = row
            self.convertMultipleSellController.removeKeyboardObserver()
            if let viewController = currentViewController {
                self.removeChildViewControllerFromContainer(viewController)
                switch row {
                case 0:
                    self.convertNormalViewController.setRateArray(self.rateArray)
                    self.convertNormalViewController.setCurrentBranchIndex(self.currentBranch)
                    self.addChildViewControllerToContainer(convertNormalViewController)
                    break
                case 1:
                    var array : [DenominationDB] = []
                    for item in self.rateArray {
                        array.append(contentsOf: item.getDominationDB())
                    }
                    self.convertMultipleSellController.setDenominationArray(array)
                    self.convertMultipleSellController.setCurrentBranchIndex(self.currentBranch)
                    self.addChildViewControllerToContainer(convertMultipleSellController)
                    self.convertMultipleSellController.addKeyboardObserver()
                    break
                case 2:
                    self.convertTwoCurrencyController.setRateArray(self.rateArray)
                    self.convertTwoCurrencyController.setCurrentBranchIndex(self.currentBranch)
                    self.addChildViewControllerToContainer(convertTwoCurrencyController)
                    break
                default:
                    break
                }
            }
        }
    }
    
    @IBAction func pickerCancel(_ sender: Any) {
        hidePicker(true)
    }
    
    @objc func changeLanguage(_ button : UIButton!) {
        if button.isSelected {          // EN -> TH
            button.isSelected = false
            AppState.sharedInstance.currentLanguage = .TH
            Localize.setCurrentLanguage("th")
        } else {                        // TH -> EN
            button.isSelected = true
            AppState.sharedInstance.currentLanguage = .EN
            Localize.setCurrentLanguage("en")
        }
        UserDefaults.standard.set(AppState.sharedInstance.currentLanguage.rawValue, forKey: "language")
    }
    
    @objc func setText() {
        modeButton.text = modeArray[currentMode].localized()
        dateLabel.text = getDateLabelText(self.refreshTime)
        timeLabel.text = getTimeLabelText(self.refreshTime)
        if branchArray.count > pickerRow {
            branchButton.text = branchArray[pickerRow].name[AppState.sharedInstance.currentLanguage.rawValue]
        }
        self.converterPickerView.reloadAllComponents()
        self.navigationItem.title = "Convert".localized()
    }
    
    func getDateLabelText(_ datetime: Date?) -> String {
        if let datetime = datetime {
            let formatter = DateFormatter()
            formatter.dateFormat = "dd-MM-yyyy"
            let time = formatter.string(from: datetime)
            let local = "Date: %@".localized()
            return String.init(format: local, time)
        } else {
            return "Date:".localized()
        }
    }
    
    func getTimeLabelText(_ datetime: Date?) -> String {
        if let datetime = datetime {
            let formatter = DateFormatter()
            formatter.dateFormat = "HH:mm"
            let time = formatter.string(from: datetime)
            let local = "Time: %@".localized()
            return String.init(format: local, time)
        } else {
            return "Time:".localized()
        }
    }
    
    func setupNavigationButton() {
        let logo = UIButton(type: .custom)
        let logoImage = UIImage(named: "logo_linda")
        logo.sizeToFit()
        
        languageButton.sizeToFit()
        languageButton.setImage(UIImage(named: "bt_language_th"), for: .normal)
        languageButton.setImage(UIImage(named: "bt_language_en"), for: .selected)
        
        languageButton.addTarget(self, action: #selector(changeLanguage(_:)), for: .touchUpInside)
        languageButton.imageView?.contentMode = .scaleAspectFit
        
        setupLanguageButton();
        let languageItem = UIBarButtonItem(customView: languageButton)
        
        logo.setImage(logoImage, for: .normal)
        logo.imageView?.contentMode = .scaleAspectFit
        let logoItem = UIBarButtonItem(customView: logo)
        self.navigationItem.setLeftBarButtonItems([logoItem], animated: false)
        self.navigationItem.setRightBarButtonItems([languageItem], animated: false)
    }
}
