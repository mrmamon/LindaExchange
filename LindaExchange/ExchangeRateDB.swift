//
//  ExchangeRateDB.swift
//  LindaExchange
//
//  Created by Milion Sun on 10/7/2560 BE.
//  Copyright © 2560 LindaExchange. All rights reserved.
//

import Foundation
import FirebaseDatabase

struct ExchangeRateDB {
    let key : String
    let countryName : String
    let currencyName : String
    let flag : String
    let rate : [Denomination]
    init(_ snapshot : DataSnapshot) {
        key = snapshot.key
        
        let snapshotValue = snapshot.value as! [String : AnyObject]
        
        if let countryName = snapshotValue["countryname"] as? String {
            self.countryName = countryName
        } else {
            self.countryName = ""
        }
        
        if let currencyName = snapshotValue["currencyname"] as? String {
            self.currencyName = currencyName
        } else {
            self.currencyName = ""
        }
        
        if let flag = snapshotValue["flag"] as? String {
            self.flag = flag
        } else {
            self.flag = ""
        }
        
        if let rateSnapshot = snapshotValue["rate"] as? [[String : AnyObject]] {
            var rate : [Denomination] = []
            for item in rateSnapshot {
                let newRate = Denomination(item)
                rate.append(newRate)
            }
            self.rate = rate
        } else {
            self.rate = []
        }
    }
    
    func getDominationDB() -> [DenominationDB] {
        var array : [DenominationDB] = []
        for eachRate in rate {
            let newDB = DenominationDB(countryName: self.countryName, currencyName: self.currencyName, flag: self.flag, denominationName: eachRate.denominationName, buy: eachRate.denominationRate.buy, sell: eachRate.denominationRate.sell)
            array.append(newDB)
        }
        return array
    }
}

struct Denomination {
//    let key : String
    let denominationName : String
    let denominationRate : DenominationRate
    
    init () {
//        self.key = ""
        self.denominationName = ""
        self.denominationRate = DenominationRate()
    }
    
    init (_ snapshotValue : [String : AnyObject]) {
//        self.key = snapshot.key
        
//        let snapshotValue = snapshot.value as! [String : AnyObject]
        
        if let denominationName = snapshotValue["denominationname"] as? String {
            self.denominationName = denominationName
        } else {
            self.denominationName = ""
        }
        
        if let denominationRate = snapshotValue["denominationrate"] as? [[String : String]] {
            self.denominationRate = DenominationRate(denominationRate)
        } else {
            self.denominationRate = DenominationRate()
        }
    }
}

struct DenominationRate {
    var buy : [String] = []
    var sell : [String] = []
    
    init () {
        self.buy.append("")
        self.sell.append("")
    }
    
    init (_ snapshotArray : [[String : String]]) {
        for snapshotValue in snapshotArray {
            self.buy.append(snapshotValue["buy"]!)
            self.sell.append(snapshotValue["sell"]!)
        }
    }
}

struct DenominationDB {
    let countryName : String
    let currencyName : String
    let flag : String
    let denominationName : String
    let buy : [String]
    let sell : [String]
    
    init(countryName : String, currencyName : String, flag : String, denominationName : String, buy : [String], sell : [String]) {
        self.countryName = countryName
        self.currencyName = currencyName
        self.flag = flag
        self.denominationName = denominationName
        self.buy = buy
        self.sell = sell
    }
}
