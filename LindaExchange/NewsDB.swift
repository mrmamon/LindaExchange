//
//  NewsDB.swift
//  LindaExchange
//
//  Created by Milion Sun on 9/20/2560 BE.
//  Copyright © 2560 LindaExchange. All rights reserved.
//

import Foundation
import FirebaseDatabase

struct NewsDB {
    let key : String
    var epoch : [Double] = []
    var photo : [String] = []
    var topic : [String] = []
    var detail : [String] = []
    
    init(_ snapshot : DataSnapshot) {
        key = snapshot.key
        
        let snapshotArray = snapshot.value as! [[String : AnyObject]]
        
        for snapshotValue in snapshotArray {
            if let epoch = snapshotValue["epoch"] as? Double {
//                let epochInt = Int(epoch)
                self.epoch.append(epoch)
            } else {
                self.epoch.append(0)
            }
            
            if let photo = snapshotValue["photo"] as? String {
                self.photo.append(photo)
            } else {
                self.photo.append("")
            }
            
            if let topic = snapshotValue["topic"] as? String {
                self.topic.append(topic)
            } else {
                self.topic.append("")
            }
            
            if let detail = snapshotValue["detail"] as? String {
                self.detail.append(detail)
            } else {
                self.detail.append("")
            }
        }
    }
}
