//
//  HomeNewsPagerViewController.swift
//  LindaExchange
//
//  Created by Milion Sun on 11/8/2560 BE.
//  Copyright © 2560 LindaExchange. All rights reserved.
//

import UIKit

class HomeNewsPagerViewController: UIPageViewController, UIPageViewControllerDataSource, UIPageViewControllerDelegate {
//    lazy var orderedViewControllers: [HomeNewsPageViewController] = {
//        return [self.newViewController(index: 0),
//                self.newViewController(index: 1),
//                self.newViewController(index: 2)]
//    } ()
    weak var newsPagerDelegate: NewsPagerDelegate?
    var orderedViewControllers: [HomeNewsPageViewController] = []
    var newsArray: [NewsDB] = []
    var timer : Timer?
    var language = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        dataSource = self
        delegate = self
        if let firstViewController = orderedViewControllers.first {
            setViewControllers([firstViewController], direction: .forward, animated: true, completion: { (complete) in
//                firstViewController
                if complete {
                    DispatchQueue.main.async {
                        self.setViewControllers([firstViewController], direction: .forward, animated: false, completion: nil)
                    }
                }
            })
        }
        self.updatePageCount(0)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func newViewController() -> HomeNewsPageViewController {
        let viewController = UIStoryboard(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "NewsPage") as! HomeNewsPageViewController
        return viewController
    }
    
    func setNews(news: [NewsDB]) {
        self.newsArray = news
        for newsDB in news {
            let viewController = self.newViewController()
            viewController.setNews(newsDB)
            viewController.language = self.language
            orderedViewControllers.append(viewController)
        }
        orderedViewControllers.reverse()
        if let firstViewController = orderedViewControllers.first {
            setViewControllers([firstViewController], direction: .forward, animated: true, completion: { (complete) in
                if complete {
                    DispatchQueue.main.async {
                        self.setViewControllers([firstViewController], direction: .forward, animated: false, completion: nil)
                    }
                }
            })
        }
        self.updatePageCount(news.count)
        setupTimer()
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        guard let viewControllerIndex = orderedViewControllers.index(of: viewController as! HomeNewsPageViewController) else {
            return nil
        }
        let previousIndex = viewControllerIndex - 1
        guard previousIndex >= 0 else {
            return orderedViewControllers.last
        }
        guard orderedViewControllers.count > previousIndex else {
            return nil
        }
        return orderedViewControllers[previousIndex]
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        guard let viewControllerIndex = orderedViewControllers.index(of: viewController as! HomeNewsPageViewController) else {
            return nil
        }
        let nextIndex = viewControllerIndex + 1
        let orderCount = orderedViewControllers.count
        guard orderCount != nextIndex else {
            return orderedViewControllers.first
        }
        guard orderCount > nextIndex else {
            return nil
        }
        return orderedViewControllers[nextIndex]
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        if let firstViewController = viewControllers?.first as? HomeNewsPageViewController, let index = orderedViewControllers.index(of: firstViewController) {
            updatePageIndex(index)
            setupTimer()
        }
    }
    
    func setupTimer() {
        if let t = timer {
            t.invalidate()
        }
        timer = Timer.scheduledTimer(timeInterval: 4, target: self, selector: #selector(moveToNextPage), userInfo: nil, repeats: true)
    }
    
    func updatePageCount(_ count: Int) {
        if let delegate = self.newsPagerDelegate {
            delegate.newsPagerViewController(newsPagerController: self, didUpdatePageCount: count)
        }
    }
    
    func updatePageIndex(_ index: Int) {
        if let delegate = self.newsPagerDelegate {
            delegate.newsPagerViewController(newsPagerController: self, didUpdatePageIndex: index)
        }
    }
    
    @objc func moveToNextPage() {
        if let firstViewController = viewControllers?.first as? HomeNewsPageViewController, let nextPage = self.pageViewController(self, viewControllerAfter: firstViewController) as? HomeNewsPageViewController {
            setViewControllers([nextPage], direction: .forward, animated: true) { (complete) in
                if let index = self.orderedViewControllers.index(of: nextPage) {
                    self.updatePageIndex(index)
                }
                if complete {
                    DispatchQueue.main.async {
                        self.setViewControllers([nextPage], direction: .forward, animated: false, completion: nil)
                    }
                }
            }
        }
    }
}

protocol NewsPagerDelegate: class {
    func newsPagerViewController(newsPagerController: HomeNewsPagerViewController, didUpdatePageCount count: Int)
    func newsPagerViewController(newsPagerController: HomeNewsPagerViewController, didUpdatePageIndex index: Int)
}
