//
//  ConvertSelectTableViewCell.swift
//  LindaExchange
//
//  Created by Milion Sun on 10/29/2560 BE.
//  Copyright © 2560 LindaExchange. All rights reserved.
//

import UIKit
import Kingfisher

class ConvertSelectTableViewCell: UITableViewCell {
    @IBOutlet var convertCheckBox: Checkbox!
    @IBOutlet var denominationLabel: UILabel!
    @IBOutlet var countryLabel: UILabel!
    @IBOutlet var flagImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        convertCheckBox.isEnabled = false
        convertCheckBox.borderStyle = .circle
        convertCheckBox.checkedBorderColor = UIColor(red: CGFloat(32.0) / 255.0, green: CGFloat(57.0) / 255.0, blue: CGFloat(110.0) / 255.0, alpha: 1.0)
//        convertCheckBox.checkedBorderColor = .green
        convertCheckBox.checkboxBackgroundColor = UIColor(red: CGFloat(32.0) / 255.0, green: CGFloat(57.0) / 255.0, blue: CGFloat(110.0) / 255.0, alpha: 1.0)
//        convertCheckBox.uncheckedBackgroundColor = convertCheckBox.uncheckedBorderColor
        convertCheckBox.uncheckedBorderColor = UIColor(red: CGFloat(32.0) / 255.0, green: CGFloat(57.0) / 255.0, blue: CGFloat(110.0) / 255.0, alpha: 1.0)
        convertCheckBox.checkmarkColor = .white
        convertCheckBox.checkmarkStyle = .tick
//        convertCheckBox.borderWidth = 3
//        convertCheckBox.checkmarkSize = 0.8
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setFlag(_ string: String) {
        if let checkedUrl = URL(string: string) {
            downloadImage(url: checkedUrl, imageView: flagImageView)
        }
    }
    
    func downloadImage(url: URL, imageView: UIImageView) {
        imageView.kf.setImage(with: url)
    }
}
