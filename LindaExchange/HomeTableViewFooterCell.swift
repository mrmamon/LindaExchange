//
//  HomeTableViewFooterCell.swift
//  LindaExchange
//
//  Created by Milion Sun on 11/1/2560 BE.
//  Copyright © 2560 LindaExchange. All rights reserved.
//

import UIKit

class HomeTableViewFooterCell: UITableViewCell {
    @IBOutlet var favoriteLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
