//
//  NewsViewController.swift
//  LindaExchange
//
//  Created by Milion Sun on 10/7/2560 BE.
//  Copyright © 2560 LindaExchange. All rights reserved.
//

import UIKit
import Localize_Swift
import Kingfisher

class NewsViewController: UIViewController {
    @IBOutlet var newsImageView: UIImageView!
    @IBOutlet var newsTextView: UITextView!
    @IBOutlet var newsTimeLabel: UILabel!
    var news : NewsDB?
    var languageButton = UIButton(type: .custom)
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.font: UIFont(name: "Calibri", size: 20)!, NSAttributedStringKey.foregroundColor: UIColor.white]
        self.navigationController?.navigationBar.tintColor = UIColor.white
        
//        setText()
        NotificationCenter.default.addObserver(self, selector: #selector(setText), name: NSNotification.Name( LCLLanguageChangeNotification), object: nil)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setText()
        setupLanguageButton()
        NotificationCenter.default.addObserver(self, selector: #selector(setText), name: NSNotification.Name( LCLLanguageChangeNotification), object: nil)
    }
    
    override func viewWillLayoutSubviews() {
        setupNavigationButton()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self)
    }
    
    func setupLanguageButton() {
        if AppState.sharedInstance.currentLanguage == .TH {
            languageButton.isSelected = false
        } else if AppState.sharedInstance.currentLanguage == .EN {
            languageButton.isSelected = true
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func downloadImage(url: URL, imageView: UIImageView) {
        imageView.kf.setImage(with: url)
    }
    
    @objc func setText() {
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineSpacing = 3
        let textColor = UIColor(red: 19 / 255.0, green: 41 / 255.0, blue: 78 / 255.0, alpha: 1.0)
//        let font = UIFont(name: "FreesiaUPC", size: 21.0)!
        let font = UIFont.systemFont(ofSize: 15.0)
        let attributes = [NSAttributedStringKey.paragraphStyle : paragraphStyle,
                          NSAttributedStringKey.foregroundColor : textColor,
                          NSAttributedStringKey.font : font]
        
        let headerParagraphStyle = NSMutableParagraphStyle()
        headerParagraphStyle.lineSpacing = 5
//        let headerFont = UIFont(name: "FreesiaUPCBold", size: 33.0)!
        let headerFont = UIFont.boldSystemFont(ofSize: 25.0)
        let headerAttributes = [NSAttributedStringKey.paragraphStyle : headerParagraphStyle,
                                NSAttributedStringKey.foregroundColor : textColor,
                                NSAttributedStringKey.font : headerFont]
        
        if let news = news {
            let headerAttributedText = NSAttributedString(string: news.topic[AppState.sharedInstance.currentLanguage.rawValue], attributes: headerAttributes)
            let mutableAttributedText = NSMutableAttributedString(attributedString: headerAttributedText)
            mutableAttributedText.append(NSAttributedString(string: "\n" + news.detail[AppState.sharedInstance.currentLanguage.rawValue], attributes: attributes))
            newsTextView.attributedText = mutableAttributedText
            
            var url = news.photo[AppState.sharedInstance.currentLanguage.rawValue]
            if !isEscaped(url) {
                url = url.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
            }
            
            if let checkedUrl = URL(string: url) {
                downloadImage(url: checkedUrl, imageView: newsImageView)
            }
            
            let time = news.epoch[AppState.sharedInstance.currentLanguage.rawValue]
            let date = Date(timeIntervalSince1970: time)
            let formatter = DateFormatter()
            formatter.dateFormat = "HH.mm : dd/MM/yyyy"
            newsTimeLabel.text = formatter.string(from: date)
        }
    }
    
    @objc func changeLanguage(_ button : UIButton!) {
        if button.isSelected {          // EN -> TH
            button.isSelected = false
            AppState.sharedInstance.currentLanguage = .TH
            Localize.setCurrentLanguage("th")
        } else {                        // TH -> EN
            button.isSelected = true
            AppState.sharedInstance.currentLanguage = .EN
            Localize.setCurrentLanguage("en")
        }
        UserDefaults.standard.set(AppState.sharedInstance.currentLanguage.rawValue, forKey: "language")
    }
    
    func setupNavigationButton() {
        languageButton.sizeToFit()
        languageButton.setImage(UIImage(named: "bt_language_th"), for: .normal)
        languageButton.setImage(UIImage(named: "bt_language_en"), for: .selected)
        
        languageButton.addTarget(self, action: #selector(changeLanguage(_:)), for: .touchUpInside)
        languageButton.imageView?.contentMode = .scaleAspectFit
        
        setupLanguageButton();
        let languageItem = UIBarButtonItem(customView: languageButton)
        self.navigationItem.setRightBarButtonItems([languageItem], animated: false)
    }
    
    func isEscaped(_ url: String) -> Bool {
        return url != url.removingPercentEncoding
    }
}
