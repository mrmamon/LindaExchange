//
//  PhoneCallDelegate.swift
//  LindaExchange
//
//  Created by Milion Sun on 8/30/2560 BE.
//  Copyright © 2560 LindaExchange. All rights reserved.
//

import UIKit

class PhoneCallDelegate {
    public static func phoneCall() {
        let number = "0863519205"
        phoneCall(number)
    }
    
    public static func phoneCall(_ number: String) {
        let detector = try! NSDataDetector(types: NSTextCheckingResult.CheckingType.phoneNumber.rawValue)
        let matches = detector.matches(in: number, options: [], range: NSRange(location: 0, length: number.utf16.count))
        guard let match = matches.first, let range = Range(match.range, in: number)
            else { return }
        let phoneNumber = String(number[range]).replacingOccurrences(of: " ", with: "")
        guard let tel = URL(string: "tel://\(phoneNumber)") else {
            return
        }
        UIApplication.shared.open(tel, options: [:], completionHandler: nil)
    }
}
