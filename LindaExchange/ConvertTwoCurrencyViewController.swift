//
//  ConvertTwoCurrencyViewController.swift
//  LindaExchange
//
//  Created by Milion Sun on 10/31/2560 BE.
//  Copyright © 2560 LindaExchange. All rights reserved.
//

import UIKit
import Localize_Swift

class ConvertTwoCurrencyViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource, UITextFieldDelegate {
    let pickerView = UIPickerView()
    var rateArray : [DenominationDB] = []
    var pickerCurrencyIndex = 0
    var pickerDenominationIndex = 0
    @IBOutlet var leftCurrencyTextField: UITextField!
    @IBOutlet var rightCurrencyTextField: UITextField!
    @IBOutlet var leftAmountTextField: UITextField!
//    @IBOutlet var leftCurrencyLabel: UILabel!
    @IBOutlet var rightAmountTextField: UITextField!
//    @IBOutlet var rightCurrencyLabel: UILabel!
    @IBOutlet var leftRateDetailLabel: UILabel!
    @IBOutlet var rightRateDetailLabel: UILabel!
    @IBOutlet var totalTextLabel: UILabel!
    @IBOutlet var totalLabel: UILabel!
    @IBOutlet var totalView: UIView!
    @IBOutlet var leftEquationTextView: UITextView!
    @IBOutlet var rightEquationTextView: UITextView!
    @IBOutlet var leftExplainationLabel: UILabel!
    @IBOutlet var rightExplainationLabel: UILabel!
    var currentCurrencyTextField : UITextField?
    var leftIndex = 0
    var rightIndex = 0
    var currentBranch = 0
    var keyboardMove : CGFloat = 0.0
    var pickerIndex = 0
    var total : Double = 0.0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        view.addGestureRecognizer(tap)
        leftAmountTextField.addTarget(self, action: #selector(amountChanged(_:)), for: UIControlEvents.editingChanged)
        rightAmountTextField.addTarget(self, action: #selector(amountChanged(_:)), for: UIControlEvents.editingChanged)

        // Do any additional setup after loading the view.
        pickerView.delegate = self
        pickerView.dataSource = self
        leftCurrencyTextField.inputView = pickerView
        rightCurrencyTextField.inputView = pickerView
        
        let toolbar = UIToolbar()
        toolbar.barStyle = .default
        toolbar.isTranslucent = true
        toolbar.sizeToFit()
        
        let doneButton = UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(pickerDone(_:)))
        let space = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(pickerCancel(_:)))
        toolbar.setItems([cancelButton, space, doneButton], animated: false)
        toolbar.isUserInteractionEnabled = true
        leftCurrencyTextField.inputAccessoryView = toolbar
        rightCurrencyTextField.inputAccessoryView = toolbar
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(notification:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(notification:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
        let dropdownImage1 = UIImageView(image: UIImage(named: "expand-button"))
        if let size = dropdownImage1.image?.size {
            dropdownImage1.frame = CGRect(x: 0.0, y: 0.0, width: size.width + 12.0, height: size.height)
        }
        dropdownImage1.contentMode = .center
        rightCurrencyTextField.rightView = dropdownImage1
        rightCurrencyTextField.rightViewMode = .always
        
        let dropdownImage2 = UIImageView(image: UIImage(named: "expand-button"))
        if let size = dropdownImage2.image?.size {
            dropdownImage2.frame = CGRect(x: 0.0, y: 0.0, width: size.width + 12.0, height: size.height)
        }
        dropdownImage2.contentMode = .center
        leftCurrencyTextField.rightView = dropdownImage2
        leftCurrencyTextField.rightViewMode = .always
        
        totalLabel.text = "0.00 " + "THB".localized()
        totalView.layer.borderColor = UIColor(red: 148.0 / 255.0, green: 148.0 / 255.0, blue: 148.0 / 255.0, alpha: 1).cgColor
        totalView.layer.borderWidth = 1
        
        changeCurrencyTextField(leftCurrencyTextField)
        changeCurrencyTextField(rightCurrencyTextField)
        changeRateDetail()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setText()
        NotificationCenter.default.addObserver(self, selector: #selector(setText), name: NSNotification.Name( LCLLanguageChangeNotification), object: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return rateArray.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return rateArray[row].currencyName + " " + rateArray[row].denominationName
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        pickerIndex = row
    }
    
    func changeCurrencyTextField(_ textField : UITextField) {
        if textField == leftCurrencyTextField {
            textField.text = getFullDenominationName(index: leftIndex)
//            leftCurrencyLabel.text = getCurrency(leftIndex)
        } else if textField == rightCurrencyTextField {
            textField.text = getFullDenominationName(index: rightIndex)
//            rightCurrencyLabel.text = getCurrency(rightIndex)
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == leftAmountTextField || textField == rightAmountTextField {
            if let text = textField.text {
                if let decimalIndex = text.index(of: ".") {
                    if (string == "0" || string.isEmpty) && decimalIndex.encodedOffset < range.location {
                        return true
                    }
                    
                    if string == "." {
                        return false
                    }
                }
                
                let formatter = NumberFormatter()
                formatter.numberStyle = .decimal
                formatter.usesGroupingSeparator = true
                formatter.groupingSeparator = ","
                formatter.maximumIntegerDigits = 18
                formatter.maximumFractionDigits = 30
                formatter.roundingMode = .floor
                var combinedText = text
                if let r = Range<String.Index>(range, in: combinedText) {
                    combinedText.replaceSubrange(r, with: string)
                }
                var textWithoutComma = combinedText.replacingOccurrences(of: ",", with: "")
                if let number = formatter.number(from: textWithoutComma) {
                    if let newString = formatter.string(from: number) {
                        textWithoutComma = newString
                    }
                }
                
                if string == "." && range.location == text.count {
                    textWithoutComma += "."
                }
                if textWithoutComma.isEmpty {
                    textWithoutComma = "0"
                }
                textField.text = textWithoutComma
                amountChanged(textField)
            }
        }
        return false
    }
    
    @objc func amountChanged(_ sender : UITextField) {
        var leftCalculationString = ""
        var rightCalculationString = ""
        total = 0.0
        
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineSpacing = 3
        paragraphStyle.alignment = NSTextAlignment.right
        let textColor = UIColor(red: 19 / 255.0, green: 41 / 255.0, blue: 78 / 255.0, alpha: 1.0)
        let font = UIFont(name: "FreesiaUPC", size: 20.0)!
        let attributes = [NSAttributedStringKey.paragraphStyle : paragraphStyle,
                          NSAttributedStringKey.foregroundColor : textColor,
                          NSAttributedStringKey.font : font]
        
        let formatter = NumberFormatter()
        formatter.numberStyle = .decimal
        formatter.groupingSeparator = ","
        formatter.usesGroupingSeparator = true
        
        if let leftAmountText = leftAmountTextField.text,
            let rightAmountText = rightAmountTextField.text {
            let leftNoGroupingText = leftAmountText.replacingOccurrences(of: ",", with: "")
            let rightNoGroupingText = rightAmountText.replacingOccurrences(of: ",", with: "")
//            if let text = sender.text {
//                if let num = formatter.number(from: text){
//                    if let str = formatter.string(from: num) {
//                       sender.text = str
//                    }
//                }
//            }
            if let leftAmount = formatter.number(from: leftNoGroupingText), let rightAmount = formatter.number(from: rightNoGroupingText) {
                let left = leftAmount.doubleValue
                let right = rightAmount.doubleValue
                let leftRate = getRateString(index: leftIndex, isBuy: true)
                let rightRate = getRateString(index: rightIndex, isBuy: false)
                let leftTotal = left * getRate(index: leftIndex, isBuy: true)
                let rightTotal = right * getRate(index: rightIndex, isBuy: false)
//                let stringFormat = "%.2f\nx %@\n= %.2f"
//                leftCalculationString = String.init(format: stringFormat, left, leftRate, leftTotal)
//                rightCalculationString = String.init(format: stringFormat, right, rightRate, rightTotal)
                total = rightTotal - leftTotal
                
                formatter.minimumFractionDigits = 3
                formatter.maximumFractionDigits = 3
                if let leftString = formatter.string(from: leftAmount), let rightString = formatter.string(from: rightAmount) {
                    if let leftTotalString = formatter.string(from: NSNumber(value: leftTotal)), let rightTotalString = formatter.string(from: NSNumber(value: rightTotal)) {
                        leftCalculationString = """
                            \(leftString)
                            x \(leftRate)
                            \(leftTotalString)
                            """
                        rightCalculationString = """
                            \(rightString)
                            x \(rightRate)
                            \(rightTotalString)
                            """
                    }
                }
            }
        }
//        leftEquationTextView.text = leftCalculationString
//        rightEquationTextView.text = rightCalculationString
        leftEquationTextView.attributedText = NSAttributedString(string: leftCalculationString, attributes: attributes)
        rightEquationTextView.attributedText = NSAttributedString(string: rightCalculationString, attributes: attributes)
        
        formatter.minimumFractionDigits = 2
        formatter.maximumFractionDigits = 2
        if let totalString = formatter.string(from: NSNumber(value: abs(total))) {
            totalLabel.text = totalString + " " + "THB".localized()
        } else {
            totalLabel.text = String.init(format: "%.2f " + "THB".localized(), abs(total))
        }
        
        if total > 0 {
            totalTextLabel.text = "Pay more".localized()
        } else {
            totalTextLabel.text = "Receive".localized()
        }
    }
    
    func changeRateDetail() {
        if rateArray.count > leftIndex {
            let rate = getRateString(index: leftIndex, isBuy: true)
            let currencyName = getCurrency(leftIndex)
            self.leftRateDetailLabel.text = "1 \(currencyName) : \(rate) THB"
        } else {
            self.leftRateDetailLabel.text = ""
        }
        
        if rateArray.count > rightIndex {
            let rate = getRateString(index: rightIndex, isBuy: false)
            let currencyName = getCurrency(rightIndex)
            self.rightRateDetailLabel.text = "1 \(currencyName) : \(rate) THB"
        } else {
            self.rightRateDetailLabel.text = ""
        }
    }
    
    func getRateString(index : Int, isBuy : Bool) -> String {
        if rateArray.count > index {
            let currency = rateArray[index]
            let formatter = NumberFormatter()
            formatter.numberStyle = .none
            if isBuy {
                return currency.buy[currentBranch]
            } else {
                return currency.sell[currentBranch]
            }
        }
        return ""
    }
    
    func getRate(index : Int, isBuy : Bool) -> Double {
        if rateArray.count > index {
            let currency = rateArray[index]
            let formatter = NumberFormatter()
            formatter.numberStyle = .none
            if isBuy {
                if let rateNumber = formatter.number(from: currency.buy[currentBranch]) {
                    return rateNumber.doubleValue
                }
            } else {
                if let rateNumber = formatter.number(from: currency.sell[currentBranch]) {
                    return rateNumber.doubleValue
                }
            }
        }
        return 0
    }
    
    func toTHB(_ amount : Double, rate : Double) -> Double {
        return amount * rate
    }
    
    func getCurrency(_ index : Int) -> String {
        if rateArray.count > index {
            return rateArray[index].currencyName
        } else {
            return ""
        }
    }
    
    func getFullDenominationName(index : Int) -> String {
        var text = ""
        if rateArray.count > index {
            let currency = rateArray[index]
            text += currency.currencyName
            if currency.denominationName != "" {
                text += " " + currency.denominationName
            }
        }
        return text
    }
    
    @objc func dismissKeyboard() {
        hidePicker(true)
    }
    
    func hidePicker(_ hide : Bool) {
        view.endEditing(hide)
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == leftCurrencyTextField || textField == rightCurrencyTextField {
            currentCurrencyTextField = textField
        }
        return true
    }
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        return true
    }
    
    @objc func pickerDone(_ sender: Any) {
        hidePicker(true)
        if let textField = currentCurrencyTextField {
            if textField == leftCurrencyTextField {
                self.leftIndex = pickerIndex
                amountChanged(leftAmountTextField)
            } else if textField == rightCurrencyTextField {
                self.rightIndex = pickerIndex
                amountChanged(rightAmountTextField)
            }
            changeCurrencyTextField(textField)
        }
        changeRateDetail()
    }
    
    @objc func pickerCancel(_ sender: Any) {
        hidePicker(true)
    }
    
    func setRateArray(_ array : [ExchangeRateDB]) {
        var newArray : [DenominationDB] = []
        for item in array {
            newArray.append(contentsOf: item.getDominationDB())
        }
        self.rateArray = newArray
        changeUI()
        if let textField = leftCurrencyTextField {
            changeCurrencyTextField(textField)
        }
        if let textField = rightCurrencyTextField {
            changeCurrencyTextField(textField)
        }
    }
    
    func setCurrentBranchIndex(_ branch : Int) {
        self.currentBranch = branch
        changeUI()
    }
    
    func changeUI() {
        if let _ = totalLabel, let textField = currentCurrencyTextField {
            amountChanged(textField)
        }
        if let _ = leftRateDetailLabel, let _ = rightRateDetailLabel {
            changeRateDetail()
        }
    }
    
    @objc func keyboardWillShow(notification : NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            let oldHeight = self.view.frame.size.height
            self.view.frame.size.height -= (keyboardSize.height - self.tabBarController!.tabBar.frame.size.height + keyboardMove)
            keyboardMove += self.view.frame.size.height - oldHeight
        }
    }
    
    @objc func keyboardWillHide(notification : NSNotification) {
        if let _ = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            let oldHeight = self.view.frame.size.height
            self.view.frame.size.height -= keyboardMove
            keyboardMove += self.view.frame.size.height - oldHeight
        }
    }
    
    @objc func setText() {
        leftExplainationLabel.text = "Currency you\nwould like to sell".localized();
        rightExplainationLabel.text = "Currency you\nwould like to buy".localized();
        if total > 0 {
            totalTextLabel.text = "Pay more".localized()
        } else {
            totalTextLabel.text = "Receive".localized()
        }
        totalLabel.text = String.init(format: "%.2f " + "THB".localized(), total)
    }
}
