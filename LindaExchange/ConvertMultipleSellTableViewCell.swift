//
//  ConvertMultipleSellTableViewCell.swift
//  LindaExchange
//
//  Created by Milion Sun on 10/30/2560 BE.
//  Copyright © 2560 LindaExchange. All rights reserved.
//

import UIKit

protocol ConvertCellDelegate : class {
    func moneyChanged(index : Int, left : String?, right : Double)
    func moveUp()
    func moveDown()
}

class ConvertMultipleSellTableViewCell: UITableViewCell, UITextFieldDelegate {
    @IBOutlet var leftCurrencyTextField: UITextField!
    @IBOutlet var rightCurrencyTextField: UITextField!
    @IBOutlet var leftAmountTextField: UITextField!
    @IBOutlet var rightAmountLabel: UILabel!
//    @IBOutlet var leftCurrencyLabel: UILabel!
    @IBOutlet var rateDetailLabel: UILabel!
    var rate : Double = 0.0
//    var val : Double = 0.0
    var index = 0
    weak var delegate : ConvertCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        leftAmountTextField.addTarget(self, action: #selector(leftAmountChange(_:)), for: UIControlEvents.editingChanged)
        leftAmountTextField.delegate = self
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @objc func leftAmountChange(_ sender:UITextField) {
        var leftVal = 0.0
        var newVal : Double = 0.0
        var formattedString = "0.00"
        let formatter = NumberFormatter()
        formatter.numberStyle = .decimal
        formatter.groupingSeparator = ","
        formatter.usesGroupingSeparator = true
        formatter.minimumFractionDigits = 2
        formatter.maximumFractionDigits = 2
        if var text = sender.text {
            text = text.replacingOccurrences(of: ",", with: "")
            if let left = formatter.number(from: text) {
//                sender.text = formatter.string(from: left)
                leftVal = left.doubleValue
                newVal = leftToTHB(leftVal)
            }
        }
        if let string = formatter.string(from: NSNumber(value: newVal)) {
            formattedString = string
        }
        rightAmountLabel.text = formattedString
//        rightAmountLabel.text = formattedString + " THB"
        if let delegate = delegate {
            delegate.moneyChanged(index: index, left: sender.text, right: newVal)
        }
    }
    
    func leftToTHB(_ left : Double) -> Double {
        return left * rate
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == leftAmountTextField {
            if let text = textField.text {
                if let decimalIndex = text.index(of: ".") {
                    if (string == "0" || string.isEmpty) && decimalIndex.encodedOffset < range.location {
                        return true
                    }
                    
                    if string == "." {
                        return false
                    }
                }
                
                let formatter = NumberFormatter()
                formatter.numberStyle = .decimal
                formatter.usesGroupingSeparator = true
                formatter.groupingSeparator = ","
                formatter.maximumIntegerDigits = 18
                formatter.maximumFractionDigits = 30
                formatter.roundingMode = .floor
                var combinedText = text
                if let r = Range<String.Index>(range, in: combinedText) {
                    combinedText.replaceSubrange(r, with: string)
                }
                var textWithoutComma = combinedText.replacingOccurrences(of: ",", with: "")
                if let number = formatter.number(from: textWithoutComma) {
                    if let newString = formatter.string(from: number) {
                        textWithoutComma = newString
                    }
                }
                
                if string == "." && range.location == text.count {
                    textWithoutComma += "."
                }
                if textWithoutComma.isEmpty {
                    textWithoutComma = "0"
                }
                textField.text = textWithoutComma
                leftAmountChange(textField)
            }
        }
        return false
    }
    
//    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
//        if let delegate = self.delegate {
//            delegate.moveUp()
//        }
//        return true
//    }
//
//    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
//        if let delegate = self.delegate {
//            delegate.moveDown()
//        }
//        return true
//    }
}
