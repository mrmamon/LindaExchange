//
//  BranchTableViewCell.swift
//  LindaExchange
//
//  Created by Milion Sun on 8/30/2560 BE.
//  Copyright © 2560 LindaExchange. All rights reserved.
//

import UIKit

class BranchTableViewCell: UITableViewCell {
    @IBOutlet var branchImageView: UIImageView!
    @IBOutlet var nameLabel: UILabel!
    @IBOutlet var timeLabel: UILabel!
    @IBOutlet var detailButton: UIButton!
    @IBOutlet var cardView: UIView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        cardView.layer.cornerRadius = 2
//        let bounds = CGRect(x: cardView.bounds.origin.x, y: cardView.bounds.origin.y, width: cardView.bounds.width, height: cardView.bounds.height)
        let shadowPath = UIBezierPath(roundedRect: cardView.bounds, cornerRadius: 2)
        cardView.layer.masksToBounds = false
        cardView.layer.shadowColor = UIColor.black.cgColor
        cardView.layer.shadowOffset = CGSize(width: 1, height: 1)
        cardView.layer.shadowOpacity = 0.3
        cardView.layer.shadowPath = shadowPath.cgPath
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
