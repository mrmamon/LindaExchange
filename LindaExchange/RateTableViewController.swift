//
//  RateTableViewController.swift
//  LindaExchange
//
//  Created by Milion Sun on 8/30/2560 BE.
//  Copyright © 2560 LindaExchange. All rights reserved.
//

import UIKit
import FirebaseDatabase
import Localize_Swift
import Kingfisher

class RateTableViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UIPickerViewDelegate, UIPickerViewDataSource, UITextFieldDelegate {
    var dummyName:[String] = ["USD 100", "USD 50", "USD 5-20", "USD 1", "JPY", "EUR 100", "GBP"]
    var dummyBuy:[String] = ["33.15", "33.13", "33.00", "32.55", "0.3025", "38.85", "42.75"]
    var dummySell:[String] = ["33.22", "33.22", "33.12", "33.12", "0.3040", "39.05", "43.15"]
//    var branchButton = UIBarButtonItem(title: "สำนักงานใหญ่", style: .plain, target: self, action: #selector(changeBranch(_:))) #selector(changeLanguage(_:)))
    var rateArray : [DenominationDB] = []
    var branchArray : [BranchDB] = []
    var headerCell : HomeTableViewHeaderCell?
    var currentBranch = 0
    var refreshTime : Date?
    var languageButton = UIButton(type: .custom)
    @IBOutlet var dateLabel: UILabel!
    @IBOutlet var timeLabel: UILabel!
    @IBOutlet var tableView: UITableView!
    @IBOutlet var headCountryLabel: UILabel!
    @IBOutlet var headCurrencyLabel: UILabel!
    @IBOutlet var headBuyLabel: UILabel!
    @IBOutlet var headSellLabel: UILabel!
//    @IBOutlet var branchButton: UIButton!
    @IBOutlet var branchButton: UITextField!
    var branchPickerView = UIPickerView()
    let alert = UIAlertController(title: nil, message: "Please Wait...", preferredStyle: .alert)
    var activityCount = 0
    var pickerRow = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
//        self.title = "RATE EXCHANGE"
        
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.font: UIFont(name: "Calibri", size: 20)!, NSAttributedStringKey.foregroundColor: UIColor.white]
//        self.navigationItem.rightBarButtonItems = [languageButton, branchButton]
        getRateData(self)
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        view.addGestureRecognizer(tap)
        
        branchPickerView.delegate = self
        branchPickerView.dataSource = self
        branchButton.inputView = branchPickerView
        
        let toolbar = UIToolbar()
        toolbar.barStyle = .default
        toolbar.isTranslucent = true
        toolbar.sizeToFit()
        
        let doneButton = UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(pickerDone(_:)))
        let space = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(pickerCancel(_:)))
        toolbar.setItems([cancelButton, space, doneButton], animated: false)
        toolbar.isUserInteractionEnabled = true
        branchButton.inputAccessoryView = toolbar
        
        branchButton.rightViewMode = .always
        let dropdownImage1 = UIImageView(image: UIImage(named: "expand-button"))
        if let size = dropdownImage1.image?.size {
            dropdownImage1.frame = CGRect(x: 0.0, y: 0.0, width: size.width + 12.0, height: size.height)
        }
        dropdownImage1.contentMode = .center
        branchButton.rightView = dropdownImage1
        
        NotificationCenter.default.addObserver(self, selector: #selector(setText), name: NSNotification.Name( LCLLanguageChangeNotification), object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setText()
        setupLanguageButton()
        NotificationCenter.default.addObserver(self, selector: #selector(setText), name: NSNotification.Name( LCLLanguageChangeNotification), object: nil)
    }
    
    override func viewWillLayoutSubviews() {
        setupNavigationButton()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self)
    }
    
    func setupLanguageButton() {
        if AppState.sharedInstance.currentLanguage == .TH {
            languageButton.isSelected = false
        } else if AppState.sharedInstance.currentLanguage == .EN {
            languageButton.isSelected = true
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return rateArray.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "homeCell", for: indexPath) as! HomeTableViewCell

        // Configure the cell...
        let row = indexPath.row
        let currentRate = rateArray[row]
        cell.nameLabel.text = currentRate.currencyName + " " + currentRate.denominationName
        cell.buyLabel.text = currentRate.buy[currentBranch]
        cell.sellLabel.text = currentRate.sell[currentBranch]
        cell.countryLabel.text = currentRate.countryName
//        print(cell.nameLabel.text)
        
//        var textColor = UIColor.white
//        if indexPath.row % 2 == 0 {
//            cell.backgroundColor = UIColor.gray
//        } else {
//            textColor = UIColor.black
//            cell.backgroundColor = UIColor.lightGray
//        }
//        cell.nameLabel.textColor = textColor
//        cell.buyLabel.textColor = textColor
//        cell.sellLabel.textColor = textColor
        
        if let checkedUrl = URL(string: currentRate.flag) {
            downloadImage(url: checkedUrl, imageView: cell.flagImageView)
        }

        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 44
    }
    
//    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
//        return 70
//    }
    
//    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
//        let headerCell = tableView.dequeueReusableCell(withIdentifier: "homeHeaderCell") as! HomeTableViewHeaderCell
//        self.headerCell = headerCell
//        return headerCell
//    }

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
     */
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return branchArray.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if branchArray.count > row {
            return self.branchArray[row].name[AppState.sharedInstance.currentLanguage.rawValue]
        } else {
            return ""
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        self.pickerRow = row
    }
    
    @IBAction func pickerCancel(_ sender: Any) {
        branchButton.resignFirstResponder()
        currentBranch = pickerRow
        tableView.reloadData()
        branchButton.text = branchArray[pickerRow].name[AppState.sharedInstance.currentLanguage.rawValue]
    }
    
    @IBAction func pickerDone(_ sender: Any) {
        branchButton.resignFirstResponder()
    }
    
    func downloadImage(url: URL, imageView: UIImageView) {
        imageView.kf.setImage(with: url)
        imageView.contentMode = .scaleAspectFill
    }
    
    @IBAction func getRateData(_ sender: Any) {
        startActivityIndicator()
        let ref = Database.database().reference()
        ref.child("exchangerate").observe(.value, with: { (snapshot) in
            self.rateArray.removeAll()
            for item in snapshot.children {
                let newRate = ExchangeRateDB(item as! DataSnapshot)
                self.rateArray.append(contentsOf: newRate.getDominationDB())
            }
            self.tableView.reloadData()
            ref.child("updateTime").child("0").observeSingleEvent(of: .value, with: { (timeSnapshot) in
                if let snapshotValue = timeSnapshot.value as? [String : AnyObject] {
                    if let timestamp = snapshotValue["timestamp"] as? Double {
                        let datetime = Date(timeIntervalSince1970: timestamp / 1000.0)
                        self.refreshTime = datetime
                        self.dateLabel.text = self.getDateLabelText(datetime)
                        self.timeLabel.text = self.getTimeLabelText(datetime)
                    }
                } else {
                    let datetime = Date()
                    self.refreshTime = datetime
                    self.dateLabel.text = self.getDateLabelText(datetime)
                    self.timeLabel.text = self.getTimeLabelText(datetime)
                }
                self.stopActivityIndicator()
            })
        })
        
        startActivityIndicator()
        ref.child("branch").observe(.value, with: { (snapshot) in
            self.branchArray.removeAll()
            for item in snapshot.children {
                let newBranch = BranchDB(item as! DataSnapshot)
                self.branchArray.append(newBranch)
            }
            if self.branchArray.count > self.currentBranch {
                let branch = self.branchArray[self.currentBranch]
                self.branchButton.text = branch.name[AppState.sharedInstance.currentLanguage.rawValue]
            }
            self.branchPickerView.reloadAllComponents()
            self.stopActivityIndicator()
        })
    }
    
    func startActivityIndicator() {
        activityCount += 1
        if activityCount == 1 {
            present(alert, animated: true, completion: nil)
        }
    }
    
    func stopActivityIndicator() {
        if activityCount > 0 {
            activityCount -= 1
        }
        if activityCount == 0 {
            alert.dismiss(animated: true, completion: nil)
        }
    }
    
//    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
//        dismissKeyboard()
//        branchPickerView.selectRow(currentBranch, inComponent: 0, animated: false)
//        branchPickerView.isHidden = false
//        return false
//    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
    
    @objc func changeLanguage(_ button : UIButton!) {
        if button.isSelected {          // EN -> TH
            button.isSelected = false
            AppState.sharedInstance.currentLanguage = .TH
            Localize.setCurrentLanguage("th")
        } else {                        // TH -> EN
            button.isSelected = true
            AppState.sharedInstance.currentLanguage = .EN
            Localize.setCurrentLanguage("en")
        }
        UserDefaults.standard.set(AppState.sharedInstance.currentLanguage.rawValue, forKey: "language")
    }
    
    @objc func setText() {
        headCountryLabel.text = "Country".localized()
        headCurrencyLabel.text = "Currency".localized()
        headBuyLabel.text = "Buy".localized()
        headSellLabel.text = "Sell".localized()
        dateLabel.text = getDateLabelText(self.refreshTime)
        timeLabel.text = getTimeLabelText(self.refreshTime)
        if branchArray.count > pickerRow {
            branchButton.text = branchArray[pickerRow].name[AppState.sharedInstance.currentLanguage.rawValue]
        }
        self.navigationItem.title = "Exchange Rate".localized()
    }
    
    func getDateLabelText(_ datetime: Date?) -> String {
        if let datetime = datetime {
            let formatter = DateFormatter()
            formatter.dateFormat = "dd-MM-yyyy"
            let time = formatter.string(from: datetime)
            let local = "Date: %@".localized()
            return String.init(format: local, time)
        } else {
            return "Date:".localized()
        }
    }
    
    func getTimeLabelText(_ datetime: Date?) -> String {
        if let datetime = datetime {
            let formatter = DateFormatter()
            formatter.dateFormat = "HH:mm"
            let time = formatter.string(from: datetime)
            let local = "Time: %@".localized()
            return String.init(format: local, time)
        } else {
            return "Time:".localized()
        }
    }
    
    func setupNavigationButton() {
        let logo = UIButton(type: .custom)
        let logoImage = UIImage(named: "logo_linda")
        logo.sizeToFit()
        
        languageButton.sizeToFit()
        languageButton.setImage(UIImage(named: "bt_language_th"), for: .normal)
        languageButton.setImage(UIImage(named: "bt_language_en"), for: .selected)
        
        languageButton.addTarget(self, action: #selector(changeLanguage(_:)), for: .touchUpInside)
        languageButton.imageView?.contentMode = .scaleAspectFit
        
        setupLanguageButton();
        let languageItem = UIBarButtonItem(customView: languageButton)
        
        logo.setImage(logoImage, for: .normal)
        logo.imageView?.contentMode = .scaleAspectFit
        let logoItem = UIBarButtonItem(customView: logo)
        self.navigationItem.setLeftBarButtonItems([logoItem], animated: false)
        self.navigationItem.setRightBarButtonItems([languageItem], animated: false)
    }
}
