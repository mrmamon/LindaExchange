//
//  HomeTableViewController.swift
//  LindaExchange
//
//  Created by Milion Sun on 28/8/60.
//  Copyright © พ.ศ. 2560 LindaExchange. All rights reserved.
//

import UIKit
import FirebaseDatabase
import Localize_Swift
import Kingfisher

class HomeTableViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, UIScrollViewDelegate, UIPickerViewDelegate, UIPickerViewDataSource, SelectTableDelegate {
    @IBOutlet var pageControl: UIPageControl!
    @IBOutlet var rateTableView: UITableView!
    var latestNewsArray : [NewsDB] = []
    let alert = UIAlertController(title: nil, message: "Please Wait...", preferredStyle: .alert)
    let newsCount: CGFloat = 3
    var activityCount = 0
    var rateArray : [DenominationDB] = []
    var selectedDenomination : [Int] = []
    var newSelectedDenomination : [Int] = []
    var branchArray : [BranchDB] = []
    var currentBranch = 0
    var branchPickerView = UIPickerView()
    var pickerRow = 0
    var pageLanguage = 0
    var refreshTime : Date?
    var footerLabel : UILabel?
    var languageButton = UIButton(type: .custom)
    @IBOutlet var branchButton: UITextField!
    @IBOutlet var dateLabel: UILabel!
    @IBOutlet var timeLabel: UILabel!
    @IBOutlet var headCountryLabel: UILabel!
    @IBOutlet var headCurrencyLabel: UILabel!
    @IBOutlet var headBuyLabel: UILabel!
    @IBOutlet var headSellLabel: UILabel!
    
    @IBOutlet var homeNewsContainer: UIView!
    private var homeNewsPager: HomeNewsPagerViewController = {
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        let viewController = storyboard.instantiateViewController(withIdentifier: "HomeNewsPager") as! HomeNewsPagerViewController
        return viewController
    } ()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.font: UIFont(name: "Calibri", size: 20)!, NSAttributedStringKey.foregroundColor: UIColor.white]
        
        let defaultSelection = [0, 5, 7, 12]
        UserDefaults.standard.register(defaults: ["favoriteCurrencies": defaultSelection])
        selectedDenomination = UserDefaults.standard.array(forKey: "favoriteCurrencies") as? [Int] ?? [Int]()
        
        branchPickerView.delegate = self
        branchPickerView.dataSource = self
        branchButton.inputView = branchPickerView
        
        let toolbar = UIToolbar()
        toolbar.barStyle = .default
        toolbar.isTranslucent = true
        toolbar.sizeToFit()
        
        let doneButton = UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(pickerDone(_:)))
        let space = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(pickerCancel(_:)))
        toolbar.setItems([cancelButton, space, doneButton], animated: false)
        toolbar.isUserInteractionEnabled = true
        branchButton.inputAccessoryView = toolbar
        
        branchButton.rightViewMode = .always
        let dropdownImage1 = UIImageView(image: UIImage(named: "expand-button"))
        if let size = dropdownImage1.image?.size {
            dropdownImage1.frame = CGRect(x: 0.0, y: 0.0, width: size.width + 12.0, height: size.height)
        }
        dropdownImage1.contentMode = .center
        branchButton.rightView = dropdownImage1
        
        getLastestNews()
        getRateData(self)
        
        AppState.sharedInstance.currentLanguage = AppState.Language(rawValue: UserDefaults.standard.integer(forKey: "language"))!
        if AppState.sharedInstance.currentLanguage == .TH {                               // -> TH
            Localize.setCurrentLanguage("th")
        } else if AppState.sharedInstance.currentLanguage == .EN {                        // -> EN
            Localize.setCurrentLanguage("en")
        }
        setText()
        
        NotificationCenter.default.addObserver(self, selector: #selector(setText), name: NSNotification.Name( LCLLanguageChangeNotification), object: nil)
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        view.addGestureRecognizer(tap)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setText()
        setupLanguageButton()
        NotificationCenter.default.addObserver(self, selector: #selector(setText), name: NSNotification.Name( LCLLanguageChangeNotification), object: nil)
    }
    
    override func viewWillLayoutSubviews() {
        setupNavigationButton()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self)
    }
    
    func setupLanguageButton() {
        if AppState.sharedInstance.currentLanguage == .TH {
            languageButton.isSelected = false
        } else if AppState.sharedInstance.currentLanguage == .EN {
            languageButton.isSelected = true
        }
    }
    
    func setupNavigationButton() {
        let logo = UIButton(type: .custom)
        let logoImage = UIImage(named: "logo_linda")
        logo.sizeToFit()
        
        languageButton.sizeToFit()
        languageButton.setImage(UIImage(named: "bt_language_th"), for: .normal)
        languageButton.setImage(UIImage(named: "bt_language_en"), for: .selected)
        
        languageButton.addTarget(self, action: #selector(changeLanguage(_:)), for: .touchUpInside)
        languageButton.imageView?.contentMode = .scaleAspectFit
        
        setupLanguageButton();
        let languageItem = UIBarButtonItem(customView: languageButton)
        
        logo.setImage(logoImage, for: .normal)
//        logo.imageView?.contentMode = .scaleAspectFit
        let logoItem = UIBarButtonItem(customView: logo)
        self.navigationItem.setLeftBarButtonItems([logoItem], animated: false)
        self.navigationItem.setRightBarButtonItems([languageItem], animated: false)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        if rateArray.count > 0 {
            return selectedDenomination.count
        } else {
            return 0
        }
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "homeCell", for: indexPath) as! HomeTableViewCell

        // Configure the cell...
        let row = selectedDenomination[indexPath.row]
        if rateArray.count > row {
        let currentRate = rateArray[row]
            cell.nameLabel.text = currentRate.currencyName + " " + currentRate.denominationName
            cell.buyLabel.text = currentRate.buy[currentBranch]
            cell.sellLabel.text = currentRate.sell[currentBranch]
            cell.countryLabel.text = currentRate.countryName
            
            if let checkedUrl = URL(string: currentRate.flag) {
                downloadImage(url: checkedUrl, imageView: cell.flagImageView)
            }
        }

        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 44
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let footerCell = tableView.dequeueReusableCell(withIdentifier: "homeFooterCell") as! HomeTableViewFooterCell
        footerCell.favoriteLabel.text = "★ Favorite Currency".localized()
        self.footerLabel = footerCell.favoriteLabel
        let tap = UITapGestureRecognizer(target: self, action: #selector(addMore(gesture:)))
        footerCell.addGestureRecognizer(tap)
        return footerCell
    }
    
    @IBAction func getRateData(_ sender: Any) {
        startActivityIndicator()
        let ref = Database.database().reference()
        ref.child("exchangerate").observe(.value, with: { (snapshot) in
            self.rateArray.removeAll()
            for item in snapshot.children {
                let newRate = ExchangeRateDB(item as! DataSnapshot)
                self.rateArray.append(contentsOf: newRate.getDominationDB())
            }
            self.rateTableView.reloadData()
            ref.child("updateTime").child("0").observeSingleEvent(of: .value, with: { (timeSnapshot) in
                if let snapshotValue = timeSnapshot.value as? [String : AnyObject] {
                    if let timestamp = snapshotValue["timestamp"] as? Double {
                        let datetime = Date(timeIntervalSince1970: timestamp / 1000.0)
                        self.refreshTime = datetime
                        self.dateLabel.text = self.getDateLabelText(datetime)
                        self.timeLabel.text = self.getTimeLabelText(datetime)
                    }
                } else {
                    let datetime = Date()
                    self.refreshTime = datetime
                    self.dateLabel.text = self.getDateLabelText(datetime)
                    self.timeLabel.text = self.getTimeLabelText(datetime)
                }
                self.stopActivityIndicator()
            })
        })
        
        startActivityIndicator()
        ref.child("branch").observe(.value, with: { (snapshot) in
            self.branchArray.removeAll()
            for item in snapshot.children {
                let newBranch = BranchDB(item as! DataSnapshot)
                self.branchArray.append(newBranch)
            }
            if self.branchArray.count > self.currentBranch {
                let branch = self.branchArray[self.currentBranch]
                self.branchButton.text = branch.name[AppState.sharedInstance.currentLanguage.rawValue]
            }
            self.branchPickerView.reloadAllComponents()
            self.stopActivityIndicator()
        })
    }
    
    func getDateLabelText(_ datetime: Date?) -> String {
        if let datetime = datetime {
            let formatter = DateFormatter()
            formatter.dateFormat = "dd-MM-yyyy"
            let time = formatter.string(from: datetime)
            let local = "Date: %@".localized()
            return String.init(format: local, time)
        } else {
            return "Date:".localized()
        }
    }
    
    func getTimeLabelText(_ datetime: Date?) -> String {
        if let datetime = datetime {
            let formatter = DateFormatter()
            formatter.dateFormat = "HH:mm"
            let time = formatter.string(from: datetime)
            let local = "Time: %@".localized()
            return String.init(format: local, time)
        } else {
            return "Time:".localized()
        }
    }
    
    func getLastestNews() {
        startActivityIndicator()
        let ref = Database.database().reference()
        ref.child("news").queryOrdered(byChild: "epoch").queryLimited(toLast: UInt(newsCount)).observe(.value, with: { (snapshot) in
            self.latestNewsArray.removeAll()
            for item in snapshot.children {
                let newNews = NewsDB(item as! DataSnapshot)
                self.latestNewsArray.append(newNews)
            }
            self.setupHomeNews()
            self.stopActivityIndicator()
        })
    }
    
    func setupHomeNews() {
        if self.latestNewsArray.count > 0 {
            homeNewsPager.setNews(news: self.latestNewsArray)
            homeNewsPager.language = self.pageLanguage
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let vc = segue.destination as? HomeNewsPagerViewController, segue.identifier == "HomeNewsPager" {
            vc.newsPagerDelegate = self
            self.homeNewsPager = vc
            print("segue")
        }
    }
    
    func startActivityIndicator() {
        activityCount += 1
        if activityCount == 1 {
            present(alert, animated: true, completion: nil)
        }
    }
    
    func stopActivityIndicator() {
        if activityCount > 0 {
            activityCount -= 1
        }
        if activityCount == 0 {
            alert.dismiss(animated: true, completion: nil)
        }
    }
    
    func downloadImage(url: URL, imageView: UIImageView) {
        imageView.kf.setImage(with: url)
        imageView.contentMode = .scaleAspectFill
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return branchArray.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        print("Language \(AppState.sharedInstance.currentLanguage.rawValue)")
        return branchArray[row].name[AppState.sharedInstance.currentLanguage.rawValue]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        pickerRow = row
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
    
    func hidePicker(_ hide : Bool) {
        branchButton.resignFirstResponder()
    }
    
    @IBAction func pickerDone(_ sender: Any) {
        hidePicker(true)
        currentBranch = pickerRow
        rateTableView.reloadData()
        print("Language \(AppState.sharedInstance.currentLanguage.rawValue)")
        branchButton.text = branchArray[pickerRow].name[AppState.sharedInstance.currentLanguage.rawValue]
    }
    
    @IBAction func pickerCancel(_ sender: Any) {
        hidePicker(true)
    }
    
    @objc func changeLanguage(_ button : UIButton!) {
        if button.isSelected {          // EN -> TH
            button.isSelected = false
            AppState.sharedInstance.currentLanguage = .TH
            Localize.setCurrentLanguage("th")
        } else {                        // TH -> EN
            button.isSelected = true
            AppState.sharedInstance.currentLanguage = .EN
            Localize.setCurrentLanguage("en")
        }
        UserDefaults.standard.set(AppState.sharedInstance.currentLanguage.rawValue, forKey: "language")
    }
    
    @objc func setText() {
        print("SetText \(AppState.sharedInstance.currentLanguage.rawValue) \(Locale.current.identifier) \("Country".localized(using: nil, in: .main))")
        headCountryLabel.text = "Country".localized()
        headCurrencyLabel.text = "Currency".localized()
        headBuyLabel.text = "Buy".localized()
        headSellLabel.text = "Sell".localized()
        dateLabel.text = getDateLabelText(self.refreshTime)
        timeLabel.text = getTimeLabelText(self.refreshTime)
        if let footer = footerLabel {
            footer.text = "★ Favorite Currency".localized()
        }
//        setupHomeNews()
        if branchArray.count > pickerRow {
            branchButton.text = branchArray[pickerRow].name[AppState.sharedInstance.currentLanguage.rawValue]
        }
    }
    
    @objc func addMore(gesture : UITapGestureRecognizer) {
        self.newSelectedDenomination = self.selectedDenomination
        let alert = UIAlertController(title: "Set Currencies".localized(), message: "", preferredStyle: .alert)
        let cancelAction = UIAlertAction(title: "Cancel".localized(), style: .destructive) { (action) in
            self.newSelectedDenomination = self.selectedDenomination
        }
        let okAction = UIAlertAction(title: "Save".localized(), style: .default) { (action) in
            self.selectedDenomination = self.newSelectedDenomination
            UserDefaults.standard.set(self.selectedDenomination, forKey: "favoriteCurrencies")
            self.rateTableView.reloadData()
        }
        alert.addAction(cancelAction)
        alert.addAction(okAction)
        
        let storyBoard = UIStoryboard(name: "Main", bundle: Bundle.main)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "ConvertSelect") as! ConvertSelectTableViewController
        viewController.denominationArray = self.rateArray
        viewController.selectedDenomination = translateSelectedDenominationArray()
        viewController.preferredContentSize = CGSize(width: 272, height: 176)
        viewController.delegate = self
        alert.setValue(viewController, forKey: "contentViewController")
        present(alert, animated: true)
    }
    
    func translateSelectedDenominationArray() -> [Bool] {
        var array : [Bool] = []
        for index in 0..<self.rateArray.count {
            if self.selectedDenomination.contains(index) {
                array.append(true)
            } else {
                array.append(false)
            }
        }
        return array
    }
    
    func finishSelection(_ array: [Bool]) {
        let newArray = array.enumerated().filter{$0.element == true}.map{$0.offset}
        self.newSelectedDenomination = newArray
    }
}

extension HomeTableViewController: NewsPagerDelegate {
    func newsPagerViewController(newsPagerController: HomeNewsPagerViewController, didUpdatePageCount count: Int) {
        pageControl.numberOfPages = count
    }
    
    func newsPagerViewController(newsPagerController: HomeNewsPagerViewController, didUpdatePageIndex index: Int) {
        pageControl.currentPage = index
    }
    
    
}
