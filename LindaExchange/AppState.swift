//
//  AppState.swift
//  LindaExchange
//
//  Created by Milion Sun on 11/1/2560 BE.
//  Copyright © 2560 LindaExchange. All rights reserved.
//

import Foundation

final class AppState {
    static let sharedInstance = AppState()
    private init() {}
    
    var currentLanguage : Language = .TH
    
    enum Language : Int {
        case TH = 0
        case EN = 1
    }
}
